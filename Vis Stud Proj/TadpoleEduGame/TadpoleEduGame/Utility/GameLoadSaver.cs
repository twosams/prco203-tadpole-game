﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using TadpoleEduGame.Utility;

using System.Xml.Serialization;
using System.IO;
using System.Xml;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

namespace TadpoleEduGame.Utility
{
    public class GameLoadSaver
    {
        string saveData;
        SaveItem saveItem;
        BinaryFormatter binaryFormatter;
        MemoryStream memoryStream;

        /// <summary>
        /// Constructor for saving
        /// </summary>
        /// <param name="saveItem">The save item to be saved to</param>
        public GameLoadSaver(SaveItem saveItem)
        {
            this.saveItem = saveItem;
            Setup();
        }

        /// <summary>
        /// Constructor for loading
        /// </summary>
        public GameLoadSaver()
        {
            Setup();
        }

        private void Setup()
        {
            //Needed for both loading and saving
            binaryFormatter = new BinaryFormatter();
            memoryStream = new MemoryStream();
        }

        /// <summary>
        /// Returns the saved SaveItem
        /// </summary>
        /// <returns></returns>
        public SaveItem LoadSavedData()
        {
            saveData = PlayerPrefs.GetString("saveData");

            byte[] readInBuff = Convert.FromBase64String(saveData);

            memoryStream.Read(readInBuff,0,readInBuff.Length);

            return binaryFormatter.Deserialize(memoryStream) as SaveItem;
        }

        /// <summary>
        /// Saves the SaveItem passed in the constructor
        /// </summary>
        public void SaveData()
        {
            if (saveItem == null)
            {
                throw new ArgumentNullException("SaveItem item is null");
            }
            binaryFormatter.Serialize(memoryStream, saveItem);
            saveData = System.Convert.ToBase64String(memoryStream.ToArray());
            PlayerPrefs.SetString("saveData", saveData);
        }
    }
}