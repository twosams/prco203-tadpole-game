﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TadpoleEduGame.Utility
{
    public static class PositionUtility
    {
        static public Vector3 NewRandomTarget()
        {
            System.Random random = new System.Random(Guid.NewGuid().GetHashCode());

            int x = random.Next(50);
            int y = random.Next(25);

            int chanceNegx = random.Next(100);
            int chanceNegy = random.Next(100);

            if(chanceNegx > 50)
            {
                x *= -1;
            }

            if (chanceNegy > 50)
            {
                y *= -1;
            }

            Vector3 value = new Vector3(x, y, random.Next(-9,5));

            return value;
        }

        public static double Sigmoid(double val)
        {
            return 1 / (1 + Math.Exp(-val));
        }

        public static Vector3 LimitPositionTo(float maxX, float maxY, float maxZ, float minX, float minY, float minZ, Vector3 position)
        {
            if (position.z > maxZ)
            {
                position.z = maxZ;
            }

            if (position.z < minZ)
            {
                position.z = minZ;
            }

            if (position.x > maxX)
            {
                position.x = maxX;
            }

            if (position.x < minX)
            {
                position.x = minX;
            }

            if (position.y > maxY)
            {
                position.y = maxY;
            }

            if (position.y < minY)
            {
                position.y = minY;
            }

            return position;
        }

        public static Vector3 OppositeDirectionTo(Vector3 fromPosition, Vector3 toPosition)
        {
                

            //Vector3 opposite = (fromPositon - toPosition);
            //opposite -= fromPositon;

            Vector3 opposite = fromPosition - toPosition;

            opposite += fromPosition;



            return opposite;
        }

    }
}
