﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TadpoleEduGame.Utility
{
    public static class NameGenerator
    {
        private static string[] names = { "Donetta" ,"Mara" ,"Ben" ,"Lorelei" ,"Hermelinda" ,"Harland" ,"Ila" ,"Peter" ,"Roma" ,"Claudio" ,"Arnita" ,"Dwight" ,"Tierra" ,"Eva" ,"Julene" ,"Cindie" ,"Goldie" ,"Darcie" ,"Romaine" ,"Moises" ,"Elmira" ,"Ronny" ,"Sam" ,"Evonne" ,"Anastacia" ,"Lettie" ,"Ciera" ,"Zelda" ,"Wilda" ,"Sarina" ,"Liana" ,"Angila" ,"Margurite" ,"Lelia" ,"Sanora" ,"Chanell" ,"James" ,"Jacquline" ,"Osvaldo" ,"Serena" ,"Dortha" ,"Julee" ,"Ellen" ,"Tonda" ,"Shan" ,"Graig" ,"Hien" ,"Viki" ,"Hyo" ,"Jayson" };
        private static Random rand = new Random(Guid.NewGuid().GetHashCode());

        public static string GetName()
        {
            return names[rand.Next(1, names.Length)];
        }
    }
}
