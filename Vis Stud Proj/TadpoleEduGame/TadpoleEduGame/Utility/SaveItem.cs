﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TadpoleEduGame.Objects;

namespace TadpoleEduGame.Utility
{
    [Serializable]
    public class SaveItem
    {
        List<Tadpole> tadpoles;
        List<Lilypad> lilypads;
        List<Dragonfly> dragonflies;

        public SaveItem(List<Tadpole> tadpoles, List<Lilypad> lilypads, List<Dragonfly> dragonflies)
        {
            this.tadpoles = tadpoles;
            this.lilypads = lilypads;
            this.dragonflies = dragonflies;
        }

        public List<Tadpole> Tadpoles
        {
            get
            {
                return tadpoles;
            }
        }

        public List<Lilypad> Lilypads
        {
            get
            {
                return lilypads;
            }
        }

        public List<Dragonfly> Dragonflies
        {
            get
            {
                return dragonflies;
            }
        }
    }
}