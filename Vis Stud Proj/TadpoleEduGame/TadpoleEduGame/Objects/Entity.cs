﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.States;
using TadpoleEduGame.Interfaces;
using UnityEngine;

namespace TadpoleEduGame.Objects
{
    /// <summary>
    /// An abstract entity in the gameworld. Implements IUpdatable and extends MonoBehaviour
    /// </summary>
    public abstract class Entity : MonoBehaviour, IUpdatable
    {
        protected IState currentState;

        /// <summary>
        /// Explicit default constructor
        /// </summary>
        public Entity()
        {
        }

        /// <summary>
        /// The entity base update where the state is updated
        /// </summary>
        public void UpdateItem()
        {
            if (currentState != null)
            {
                currentState.Update(this);
            }
        }
    }
}
