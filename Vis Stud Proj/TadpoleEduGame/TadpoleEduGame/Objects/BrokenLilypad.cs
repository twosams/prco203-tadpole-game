﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Interfaces;
using TadpoleEduGame.States.LilypadStates;
using TadpoleEduGame.States;
using UnityEngine;
using TadpoleEduGame.Utility;

namespace TadpoleEduGame.Objects
{
    class BrokenLilypad : Lilypad, IWaterMovable
    {
        //Variables
        private Rigidbody2D rigidBody;
        private SpriteRenderer spritRend;
        private float alpha = 1.0f;
        private float duration = 10.0f;


        void Start()
        {
            Setup();
        }

        private void Setup()
        {
            rigidBody = GetComponent<Rigidbody2D>();
            spritRend = GetComponent<SpriteRenderer>();

            //ChangeState(new SinkingState);
        }

        void Update()
        {
            Transparency();

            Debug.Log(alpha);

            if(alpha < 0.1f)
            {
                GameObject.Destroy(this.gameObject);
            }
        }

        private void Transparency()
        {
            float lerp = Mathf.PingPong(Time.time, duration) / duration;

            alpha = Mathf.Lerp(0, 1, lerp);

            spritRend.color = new Color(spritRend.color.r, spritRend.color.g, spritRend.color.b, alpha);
        }

        public new void MoveAway(float distanceFromWaterMove, float x, float y, float z)
        {
            Vector3 disturbanceLocation = new Vector3(x, y, x);

            if (disturbanceLocation != Vector3.zero)
            {
                Vector3 position;

                position = transform.position - disturbanceLocation;
                position.z = transform.position.z;
                position.Normalize();

                rigidBody.AddForce(position * 5, ForceMode2D.Impulse);
            }
        }
    }
}
