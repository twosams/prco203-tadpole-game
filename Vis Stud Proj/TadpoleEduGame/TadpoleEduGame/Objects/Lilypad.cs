﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Interfaces;
using TadpoleEduGame.States.LilypadStates;
using TadpoleEduGame.States;
using UnityEngine;
using TadpoleEduGame.Utility;

namespace TadpoleEduGame.Objects
{

    [Serializable]
    /// <summary>
    /// Object representing a lilypad. Implements IwaterMovable and IUpdatable and extends Entity
    /// </summary>
    public class Lilypad : Entity, IWaterMovable, IUpdatable
    {
        private int rotVal = 0;
        private Rigidbody2D rigidB;
        private Renderer lilypadRend;

        private bool tadpoleSlot = true;
        private bool dragonflySlot = true;

        /// <summary>
        /// Unity default setup method
        /// </summary>
        void Start()
        {
            Setup();
        }

        /// <summary>
        /// Explicit default constructor
        /// </summary>
        public Lilypad()
        {
            Setup();
        }

        /// <summary>
        /// Deep copy of existing lilypad
        /// </summary>
        /// <param name="lilypad">Lilypad to deep copy</param>
        public Lilypad(Lilypad lilypad)
        {
            rotVal = lilypad.rotVal;
            ChangeState((LilypadState)lilypad.currentState);
        }

        /// <summary>
        /// Helper function to avoid duplicate code in constructor and Start()
        /// </summary>
        void Setup()
        {
            ChangeState(new RottingState());
            rigidB = GetComponent<Rigidbody2D>();
            lilypadRend = GetComponent<Renderer>();
        }        

        /// <summary>
        /// The lilypad moves away from a given location
        /// </summary>
        /// <param name="distanceFromWaterMove">Distance from the lilypad to the given location</param>
        /// <param name="x">Disturbance location X</param>
        /// <param name="y">Disturbance location Y</param>
        /// <param name="z">Disturbance location Z</param>
        public void MoveAway(float distanceFromWaterMove, float x, float y, float z)
        {
            Vector3 disturbanceLocation = new Vector3(x, y, x);            

            if (disturbanceLocation != Vector3.zero)
            {
                Vector3 position;

                position = transform.position - disturbanceLocation;
                position.z = transform.position.z;
                position.Normalize();

                rigidB.AddForce(position * 5, ForceMode2D.Impulse);
            }
        }

        /// <summary>
        /// The lilypad updates its state/behaviour
        /// </summary>
        public new void UpdateItem()
        {            
            //Update from the base 'Entity' class (this updates the state)
            base.UpdateItem();
        }

        /// <summary>
        /// the lilypad changes state to the new state
        /// </summary>
        /// <param name="state">The new state</param>
        public void ChangeState(LilypadState state)
        {
            if (currentState != null)
            {
                currentState.OnExit(this);
            }
            currentState = state;
            currentState.OnEnter(this);
        }
                

        /// <summary>
        /// The lilypad's current state
        /// </summary>
        public IState State
        {
            get
            {
                return currentState;
            }
        }

        /// <summary>
        /// The current rot value of the lilypad, where 0 is not rotted at all
        /// </summary>
        public int RotVal
        {
            get
            {
                return rotVal;
            }
            set
            {
                rotVal = value;
            }
        }

        /// <summary>
        /// True if there's an available slot for a tadpole to hide, false if not
        /// </summary>
        public bool TadpoleSlot
        {
            get
            {
                return tadpoleSlot;
            }
            set
            {
                tadpoleSlot = value;
            }
        }

        /// <summary>
        /// True if there's an available slot for a dragonfly to land, false if not
        /// </summary>
        public bool DragonflySlot
        {
            get
            {
                return dragonflySlot;
            }
            set
            {
                dragonflySlot = value;
            }
        }
    }
}
