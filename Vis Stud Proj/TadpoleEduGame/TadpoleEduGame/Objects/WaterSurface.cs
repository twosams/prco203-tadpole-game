﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TadpoleEduGame.Objects
{
    /// <summary>
    /// The highest level of the water, used for limiting movement above/below the water among other things
    /// </summary>
    public class WaterSurface : Entity
    {
        //This class is used for identification of water
    }
}
