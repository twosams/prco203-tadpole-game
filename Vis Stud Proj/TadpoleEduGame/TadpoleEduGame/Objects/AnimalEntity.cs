﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Interfaces;
using UnityEngine;

namespace TadpoleEduGame.Objects
{
    [Serializable]
    /// <summary>
    /// An animal entity, implements IMovable and extends Entity
    /// </summary>
    public class AnimalEntity : Entity, IMoveable
    {
        protected float sightDistance = 5.0f;
        protected float speed = 1.0f;

        /// <summary>
        /// Does one 'tick' of frame-rate independent movement towards a given point
        /// </summary>
        /// <param name="targetPosition">The target location to head towards</param>
        public void HeadTowards(UnityEngine.Vector3 targetPosition)
        {
            this.transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
            LookAt(targetPosition);
        }

        /// <summary>
        /// The AnimalEntity looks in the direction of a location
        /// </summary>
        /// <param name="targetPosition">The location to look in the direction of</param>
        public void LookAt(UnityEngine.Vector3 targetPosition)
        {
            // get the angle
            Vector3 norTar = (targetPosition - transform.position).normalized;
            float angle = Mathf.Atan2(norTar.y, norTar.x) * Mathf.Rad2Deg;
            // rotate to angle
            Quaternion rotation = new Quaternion();
            rotation.eulerAngles = new Vector3(0, 0, angle - 90);
            transform.rotation = rotation;
        }
    }
}
