﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using TadpoleEduGame.Interfaces;

namespace TadpoleEduGame.Objects
{
    /// <summary>
    /// Object representing food for tadpoles. Implements IEdible and extends Entity
    /// </summary>
    public class Food : Entity, IEdible, IWaterMovable
    {
        private bool claimed = false;
        private Rigidbody2D foodRigid;

        /// <summary>
        /// The Food object is destroyed and the eater has it's hunger decreased if it is a tadpole
        /// </summary>
        /// <param name="eater"></param>
        public void BeEaten(Entity eater)
        {
            //Debug.Log("Being eaten!");
            System.Random random = new System.Random();
            if (eater.GetType() == typeof(Tadpole))
            {
                ((Tadpole)eater).HungerLevel -= random.NextDouble();
            }
            GameObject.Destroy(this.gameObject);
        }

        /// <summary>
        /// True if a tadpole has already identified this food to eat, false if it's free
        /// </summary>
        public bool Claimed
        {
            get
            {
                return claimed;
            }
            set
            {
                claimed = value;
            }
        }

        /// <summary>
        /// Move the food away from a point
        /// </summary>
        /// <param name="distanceFromWaterMove">The distance from the food to the water disturbance</param>
        /// <param name="x">x co-ord</param>
        /// <param name="y">y co-ord</param>
        /// <param name="z">z co-ord</param>
        public void MoveAway(float distanceFromWaterMove, float x, float y, float z)
        {
            if (foodRigid == null)
            {
                foodRigid = GetComponent<Rigidbody2D>();
            }
            if(distanceFromWaterMove < 2.5f)
            {
                Vector3 oppositeDirection = transform.position - new Vector3(x, y, z);
                foodRigid.AddForce(new Vector2(oppositeDirection.x, oppositeDirection.y), ForceMode2D.Impulse);
            }
            
        }
    }
}
