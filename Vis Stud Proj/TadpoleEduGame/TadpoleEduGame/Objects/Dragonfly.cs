﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.States.DragonflyStates;
using TadpoleEduGame.Interfaces;
using TadpoleEduGame.States.TadpoleBehaviourStates;
using UnityEngine;
using TadpoleEduGame.States.TadpoleGrowthStates;

namespace TadpoleEduGame.Objects
{
    [Serializable]
    /// <summary>
    /// A dragonfly object. Implements IUpdatable and extends AnimalEntity
    /// </summary>
    public class Dragonfly : AnimalEntity, IUpdatable, IEdible
    {
        private Lilypad targetLilypad;
        private Tadpole targetTadpole;
        private float timeSinceLastAte = 0;
        private static float minimumEatTime = 3.0f;
        /// <summary>
        /// Unity default setup method for scripts
        /// </summary>
        void Start()//When a script
        {
            //Debug.Log("Dragonfly start");
            Setup();
        }

        /// <summary>
        /// Simple constructor
        /// </summary>
        /// <param name="speed">The speed of the dragonfly</param>
        public Dragonfly(float speed)
        {
            Setup();
            this.speed = speed;
        }

        public Dragonfly(Dragonfly dragonfly)
        {
            targetLilypad = dragonfly.targetLilypad;
            targetTadpole = dragonfly.targetTadpole;
            ChangeState((DragonflyState)dragonfly.currentState);

            speed = dragonfly.speed;
        }

        /// <summary>
        /// Helper function to avoid duplicate code from Start() and the contructor(s)
        /// </summary>
        private void Setup()
        {
            this.speed = 8.0f; //arbritrary starting val
            this.sightDistance = 6.0f;
            ChangeState(new FlyingState());
        }

        /// <summary>
        /// The dragonfly updates, including updating state and therefore behaviour
        /// </summary>
        public new void UpdateItem()
        {
            timeSinceLastAte += Time.deltaTime;

            List<Lilypad> nearbyLilypads = PondEnvironment.Instance.GetNearbyLilypads(transform.position, sightDistance, true);
            if (nearbyLilypads.Count > 0)
            {
                List<Entity> entityLilypadList = new List<Entity>(nearbyLilypads.Count);
                foreach (Lilypad lilypad in nearbyLilypads)
                {
                    entityLilypadList.Add(lilypad);
                }

                Lilypad tempLilypad = null;

                while (tempLilypad == null && entityLilypadList.Count > 0) //loop to find closest lilypad that isn't taken
                {
                    tempLilypad = (Lilypad)PondEnvironment.Instance.GetClosestEntityInList(entityLilypadList, transform.position);
                    if (tempLilypad.DragonflySlot == false)
                    {
                        entityLilypadList.Remove(tempLilypad);
                        tempLilypad = null;
                    }
                    else
                    {
                        TargetLilypad = tempLilypad;
                    }
                }
            }

            List<Tadpole> nearbyTadpoles = PondEnvironment.Instance.GetNearbyTadpoles(transform.position, sightDistance);
            if (nearbyTadpoles.Count > 0)
            {
                List<Entity> entityTadpoleList = new List<Entity>(nearbyTadpoles.Count);
                foreach (Tadpole tadpole in nearbyTadpoles)
                {
                    if(tadpole.State.GetType() != typeof(GrowthStateFour))
                    {
                        entityTadpoleList.Add(tadpole);
                    }                    
                }

                do
                {
                    targetTadpole = (Tadpole)PondEnvironment.Instance.GetClosestEntityInList(entityTadpoleList, transform.position);
                } while (entityTadpoleList.Count > 0 && targetTadpole.State.GetType() == typeof(HideState));
            }
            //Update from the base 'Entity' class (this updates the state)
            base.UpdateItem();
        }

        /// <summary>
        /// The dragonfly changes to a new state
        /// </summary>
        /// <param name="state">The state to change to</param>
        public void ChangeState(DragonflyState state)
        {
            if (currentState != null)
            {
                currentState.OnExit(this);
            }
            currentState = state;
            currentState.OnEnter(this);
        }

        /// <summary>
        /// The dragonfly is destroyed if a valid eater is given, otherwise an exception is thrown
        /// </summary>
        /// <param name="eater">The thing eating the dragonfly</param>
        public void BeEaten(Entity eater)
        {
            try
            {
                Tadpole frog = (Tadpole)eater;

                if (frog.State.GetType() != typeof(GrowthStateFour))
                {
                    throw new ArgumentException("Dragonflies may only be eaten by frogs");
                }
                else
                {
                    Debug.Log("Im eaten");
                    GameObject.Destroy(this.gameObject);
                }

            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        public void Freeze()
        {
            ChangeState(new FrozenState());
        }

        /// <summary>
        /// If not null, a lilypad the dragonfly has spotted
        /// </summary>
        public Lilypad TargetLilypad
        {
            get
            {
                return targetLilypad;
            }
            set
            {
                if (value == null)
                {
                    if (targetLilypad != null)
                    {
                        targetLilypad.DragonflySlot = true;
                    }
                }
                else
                {
                    value.DragonflySlot = false;
                }

                targetLilypad = value;
            }
        }

        /// <summary>
        /// If not null, a tadpole the dragonfly has spotted
        /// </summary>
        public Tadpole TargetTadpole
        {
            get
            {
                return targetTadpole;
            }
            set
            {
                targetTadpole = value;
            }
        }

        /// <summary>
        /// The amount of time, in seconds, since the dragonfly last ate a tadpole
        /// </summary>
        public float TimeSinceLastAte
        {
            get
            {
                return timeSinceLastAte;
            }
            set
            {
                timeSinceLastAte = value;

                if (timeSinceLastAte < 0)
                {
                    timeSinceLastAte = 0;
                }
            }
        }

        public float MinimumEatTime
        {
            get
            {
                return minimumEatTime;
            }
        }

        
    }
}
