﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Interfaces;
using TadpoleEduGame.States;
using TadpoleEduGame.States.TadpoleBehaviourStates;
using TadpoleEduGame.States.TadpoleGrowthStates;
using UnityEngine;
using TadpoleEduGame.Utility;

namespace TadpoleEduGame.Objects
{
    [Serializable]
    /// <summary>
    /// An object representing a tapole. Extends IwaterMovable and IEdible, extends AnimalEntity
    /// </summary>
    public class Tadpole : AnimalEntity, IWaterMovable, IEdible
    {
        private string tadpoleName;
        private static float idealTemperature = 23.0f;
        private float temperature = 0;
        private float fearLevel;
        private double hunger; // value between 0 and 1, where 1 is most hungry
        private Dragonfly enemy = null;
        private Food targetFood = null;
        private Lilypad targetLilypad = null;

        private int sampleCounter;
        private static int samples = 120;
        private float[] temperatureSamples;

        private bool readyToEvolve = false;
        private bool doEvolve = false;

        private Vector3 tongueEndPoint;

        private float timeSinceLastAte = 0;

        private Vector3 disturbance = Vector3.zero;

        /// <summary>
        /// Default unity setup method
        /// </summary>
        void Start() //when used as script
        {
            Setup();
        }

        /// <summary>
        /// Simple constuctor
        /// </summary>
        /// <param name="speed">The speed of the tadpole</param>
        public Tadpole(float speed)
        {
            this.speed = speed;
            Setup();
        }

        /// <summary>
        /// Deep copy of an existing tadpole
        /// </summary>
        /// <param name="tadpole">Tadpole to deep copy</param>
        public Tadpole(Tadpole tadpole)
        {
            idealTemperature = tadpole.IdealTemperature;
            temperature = tadpole.temperature;
            fearLevel = tadpole.fearLevel;
            hunger = tadpole.hunger;
            enemy = tadpole.enemy;
            targetFood = tadpole.targetFood;
            targetLilypad = tadpole.targetLilypad;

            sampleCounter = tadpole.sampleCounter;
            temperatureSamples = tadpole.temperatureSamples;
            samples = tadpole.temperatureSamples.Length;

            readyToEvolve = tadpole.readyToEvolve;
            doEvolve = tadpole.doEvolve;
            ChangeState((TadpoleState)tadpole.currentState);

            speed = tadpole.speed;
        }

        /// <summary>
        /// Setup helper method to avoid duplicate code in Start() and any constructors
        /// </summary>
        private void Setup()
        {
            try
            {
                temperatureSamples = new float[samples];
                sampleCounter = 0;
                System.Random random = new System.Random(Guid.NewGuid().GetHashCode());
                hunger = random.NextDouble();
                ChangeState(new GrowthStateZero());
                ((GrowthState)currentState).ChangeBehaviourState(new SpawnState());                
                speed = 1.5f;
                sightDistance = 5.0f;
                tadpoleName = NameGenerator.GetName();
                name = "Tadpole " + tadpoleName;
            }
            catch (NullReferenceException ex)
            {
                //Debug.Log("Error in tadpole setup!");
                Debug.LogException(ex);
            }

            //DoEvolve = true;
            //ChangeState(new GrowthStateFour());
            //ChangeState(new WanderState());
        }

        /// <summary>
        /// Takes a temperature sample from the pond at the current location. After enough samples are taken the tadpole's temperature is updated.
        /// </summary>
        public void SampleTemparature()
        {
            temperatureSamples[sampleCounter] = PondEnvironment.Instance.GetTemperatureAt(transform.position);

            if (sampleCounter == samples - 1)
            {
                float total = 0;
                for (int iteration = 0; iteration < samples - 1; iteration++)
                {
                    total += temperatureSamples[iteration];
                }
                temperature = total / sampleCounter;
                sampleCounter = -1;
            }

            sampleCounter++;
        }

        /// <summary>
        /// The tadpole moves away from a given water disturbance's location
        /// </summary>
        /// <param name="distanceFromWaterMove"></param>
        /// <param name="x">Disturbance location X</param>
        /// <param name="y">Disturbance location Y</param>
        /// <param name="z">Disturbance location Z</param>
        public void MoveAway(float distanceFromWaterMove, float x, float y, float z)
        {
            KnownDisturbance = new Vector3(x, y, z);
        }

        /// <summary>
        /// The tadpole is destroyed if a dragonfly is eating it or an exception is thrown
        /// </summary>
        /// <param name="eater">The eater</param>
        public void BeEaten(Entity eater)
        {
            System.Random random = new System.Random();
            if (eater.GetType() == typeof(Dragonfly))
            {
                ((Dragonfly)eater).TimeSinceLastAte = 0.0f;
                GameObject.Destroy(this.gameObject);
            }
            else
            {
                throw new ArgumentException("Tadpoles cannot be eaten by objects that aren't Dragonflies");
            }
        }

        /// <summary>
        /// The tadpole is updated and may spot lilypads, dragonflies etc. It's state is also updated
        /// </summary>
        public new void UpdateItem()
        {
            HungerLevel += 0.01 * Time.deltaTime;
            //Update from the base 'Entity' class (this calls update on the state)
            base.UpdateItem();
        }

        /// <summary>
        /// the tadpole's state is changed
        /// </summary>
        /// <param name="state">The new state</param>
        public void ChangeState(TadpoleState state)
        {
            if (state is GrowthState)
            {
                if (currentState != null)
                {
                    currentState.OnExit(this);
                    hunger = 0.5f;
                }
                currentState = state;
                currentState.OnEnter(this);
            }
            else
            {
                try
                {
                    ((GrowthState)currentState).ChangeBehaviourState(state);
                }
                catch (Exception ex)
                {
                    Debug.LogException(ex);
                }
            }
        }

        public void Evolve()
        {
            if(ReadyToEvolve)
            {
                
                if (currentState.GetType() == typeof(GrowthStateThree))
                {
                    ChangeState(new GrowthStateFour());
                    ChangeState(new WanderState());
                    ReadyToEvolve = false;
                }
                if (currentState.GetType() == typeof(GrowthStateTwo))
                {
                    ChangeState(new GrowthStateThree());
                    ChangeState(new WanderState());
                    ReadyToEvolve = false;
                }
                if (currentState.GetType() == typeof(GrowthStateOne))
                {
                    ChangeState(new GrowthStateTwo());
                    ChangeState(new WanderState());
                    ReadyToEvolve = false;
                }
            }            
        }

        public void MoveTongue()
        {
            if (Enemy != null)
            {
                StartCoroutine(ShootTongue(this));
            }
            else
            {
                throw new NullReferenceException("No enemy when moving tongue");
            }         
        }

        private System.Collections.IEnumerator ShootTongue(Tadpole frog)
        {
            //tongueEndPoint = transform.position;
            //while (tongueEndPoint != frog.Enemy.transform.position)
            //{
            //    yield return null;
            //    if (frog.Enemy == null || TimeSinceLastAte < 3)
            //    {
            //        yield break;
            //    }
            //    tongueEndPoint = Vector3.MoveTowards(tongueEndPoint, frog.Enemy.transform.position, 0.7f);
            //}

            //if(tongueEndPoint == frog.Enemy.transform.position)
            //{
            //    frog.Enemy.Freeze();
            //}            
            //while (tongueEndPoint != frog.transform.position)
            //{
            //    yield return null;
            //    if (frog.Enemy == null)
            //    {
            //        yield break;
            //    }
            //    tongueEndPoint = Vector3.MoveTowards(tongueEndPoint, frog.transform.position, 0.7f);
            //    frog.Enemy.transform.position = tongueEndPoint;
            //}
            //try
            //{
            //    Dragonfly friendlyNeighbourhoodDragonfly = Enemy;
            //    PondEnvironment.Instance.RemoveDragonFly(frog.Enemy);
            //    friendlyNeighbourhoodDragonfly.BeEaten(frog);

            //    frog.TimeSinceLastAte = 0;
            //}
            //catch (Exception ex)
            //{
            //    Debug.LogException (ex);
            //}    

            while (tongueEndPoint != frog.Enemy.transform.position)
            {
                yield return null;
                if (frog.Enemy == null)
                {
                    yield break;
                }
                tongueEndPoint = Vector3.MoveTowards(tongueEndPoint, frog.Enemy.transform.position, 0.7f);
            }

            frog.Enemy.Freeze();


            while (tongueEndPoint != frog.transform.position)
            {
                yield return null;
                if (frog.Enemy == null)
                {
                    yield break;
                }
                tongueEndPoint = Vector3.MoveTowards(tongueEndPoint, frog.transform.position, 0.7f);
                frog.Enemy.transform.position = tongueEndPoint;
            }

            Dragonfly friendlyNeighbourhoodDragonfly = Enemy;
            PondEnvironment.Instance.RemoveDragonFly(frog.Enemy);
            friendlyNeighbourhoodDragonfly.BeEaten(frog);
        }

        /// <summary>
        /// The tadpole's level of fear where 0 is not scared
        /// </summary>
        public float FearLevel
        {
            get
            {
                return fearLevel;
            }
            set
            {
                if (fearLevel < 0)
                {
                    fearLevel = 0;
                }
                else
                {
                    fearLevel = value;
                }
            }
        }

        /// <summary>
        /// Value between 0 and 1 where 1 is most hungry
        /// </summary>
        public double HungerLevel
        {
            get
            {
                return hunger;
            }
            set
            {
                hunger = value;

                if (hunger < 0)
                {
                    hunger = 0;
                }
                else if (hunger > 1)
                {
                    hunger = 1;
                }
            }
        }

        public float TimeSinceLastAte
        {
            get
            {
                return timeSinceLastAte;
            }
            set
            {
                timeSinceLastAte = value;
            }
        }

        /// <summary>
        /// The tadpole's temperature
        /// </summary>
        public float Temperature
        {
            get
            {
                return temperature;
            }
            set
            {
                temperature = value;
            }
        }

        /// <summary>
        /// If not null, a lilypad the tadpole has spotted
        /// </summary>
        public Lilypad TargetLilypad
        {
            get
            {
                return targetLilypad;
            }
            set
            {
                targetLilypad = value;
            }
        }

        /// <summary>
        /// If not null, a dragonfly the tadpole has spotted
        /// </summary>
        public Dragonfly Enemy
        {
            get
            {
                return enemy;
            }
            set
            {
                enemy = value;
            }
        }

        /// <summary>
        /// The tadpole's current state
        /// </summary>
        public IState State
        {
            get
            {
                return currentState;
            }
        }

        /// <summary>
        /// If not null, food the tadpole has spotted
        /// </summary>
        public Food TargetFood
        {
            get
            {
                return targetFood;
            }
            set
            {
                targetFood = value;
            }
        }

        /// <summary>
        /// The distance the tadpole can see
        /// </summary>
        public float SightDistance
        {
            get
            {
                return sightDistance;
            }
            set
            {
                sightDistance = value;
            }
        }

        /// <summary>
        /// The ideal temperature of this tadpole
        /// </summary>
        public float IdealTemperature
        {
            get
            {
                return idealTemperature;
            }
        }

        /// <summary>
        /// True if this tadpole is ready to chnage into the next stage
        /// </summary>
        public bool ReadyToEvolve
        {
            get
            {
                return readyToEvolve;
            }
            set
            {
                readyToEvolve = value;
                //Debug.Log(tadpoleName + " " + value);
            }
        }

        /// <summary>
        /// True if the tadpole is free to evolve, false if the tadpole must keep it's current state
        /// </summary>
        public bool DoEvolve
        {
            get
            {
                return doEvolve;
            }
            set
            {
                doEvolve = value;
            }
        }

        /// <summary>
        /// The speed this tadpole can move at, with a lower limit of 0.1
        /// </summary>
        public float Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = value;
                if (speed <= 0) //don't have a negative speed o rno speed at all
                {
                    speed = 0.1f;
                }
            }
        }

        /// <summary>
        /// The location of a water disturbance the tadpole encountered. A Vector3.Zero if no know disturbance
        /// </summary>
        public Vector3 KnownDisturbance
        {
            get
            {
                return disturbance;
            }
            set
            {
                disturbance = value;
            }
        }

        /// <summary>
        /// The name of this tadpole
        /// </summary>
        public string Name
        {
            get
            {
                return tadpoleName;
            }
        }

        public Vector3 TongueEndLocation
        {
            get
            {
                return tongueEndPoint;
            }
        }
    }
}
