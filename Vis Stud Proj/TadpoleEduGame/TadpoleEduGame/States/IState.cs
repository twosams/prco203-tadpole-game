﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;

namespace TadpoleEduGame.States
{
    public interface IState
    {
        void OnEnter(Entity gameEntity);
        void OnExit(Entity gameEntity);
        void Update(Entity gameEntity);
    }
}
