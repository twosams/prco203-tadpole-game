﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.LilypadStates
{
    [Serializable]
    class RottedState : LilypadState
    {
        public GameObject brokenLilyPad = Resources.Load<GameObject>("Prefabs/brokenLilyPad");
        

        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Lilypad rotted");
            gameEntity.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Lilypad 3");
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {

        }

        public override void Update(Entity gameEntity)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray raycast = Camera.main.ScreenPointToRay(Input.mousePosition);

                RaycastHit2D hit = Physics2D.Raycast(raycast.origin, raycast.direction);
                if (hit.collider != null && hit.collider.gameObject.tag == "Lilypad" && hit.collider.gameObject == gameEntity.gameObject)
                {                    
                    GameObject.Instantiate(brokenLilyPad, hit.collider.transform.position, Quaternion.identity);
                    GameObject.Destroy(hit.collider.gameObject);

                    PondEnvironment.Instance.RemoveNullItemsFromLists();
                }
            }
        }
    }
}
