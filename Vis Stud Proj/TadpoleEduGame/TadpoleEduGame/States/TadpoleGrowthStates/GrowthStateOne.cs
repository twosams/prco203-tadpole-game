﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.States.TadpoleBehaviourStates;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleGrowthStates
{
    [Serializable]
    class GrowthStateOne : GrowthState
    {
        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            try
            {
                tadpole = (Tadpole)gameEntity;
                tadpole.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/tadpole 1");
                tadpole.ChangeState(new WanderState());
            }
            catch(Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
        }

        public override void Update(Entity gameEntity)
        {
            base.Update(gameEntity);
        }
    }
}
