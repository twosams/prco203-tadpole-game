﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using TadpoleEduGame.States.TadpoleBehaviourStates;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleGrowthStates
{
    class GrowthStateZero : GrowthState
    {
        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Tapdole left stage zero");
        }

        public override void Update(Entity gameEntity)
        {
            behaviourState.Update(gameEntity);
        }
    }
}
