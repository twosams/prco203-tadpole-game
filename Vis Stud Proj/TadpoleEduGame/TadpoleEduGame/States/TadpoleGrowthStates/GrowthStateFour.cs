﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using TadpoleEduGame.States.TadpoleBehaviourStates;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleGrowthStates
{
    class GrowthStateFour : GrowthState
    {
        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            try
            {
                tadpole = (Tadpole)gameEntity;
                tadpole.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/frog");
                tadpole.gameObject.transform.localScale *= 1.5f;
                tadpole.ChangeState(new WanderState());
                tadpole.SightDistance *= 2.5f;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cast error: " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            Console.WriteLine("Tadpole left stage four of growth");
        }

        public override void Update(Entity gameEntity)
        {
            base.Update(gameEntity);
            Tadpole frog = (Tadpole)gameEntity;

            if (behaviourState.GetType() == typeof(MoveToSurfaceState))
            {
                frog.ChangeState(new WanderState());
            }

            if(behaviourState.GetType() == typeof (FleeState))
            {
                frog.ChangeState(new WanderState());
            }
        }
    }
}
