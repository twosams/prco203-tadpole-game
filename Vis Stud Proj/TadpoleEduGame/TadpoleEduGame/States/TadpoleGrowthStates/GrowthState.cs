﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.States.TadpoleBehaviourStates;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleGrowthStates
{
    [Serializable]
    class GrowthState : TadpoleState
    {
        protected TadpoleState behaviourState;
        protected Tadpole tadpole;

        public void ChangeBehaviourState(TadpoleState state)
        {
            if (behaviourState != null)
            {
                behaviourState.OnExit(tadpole);
            }
            behaviourState = state;
            behaviourState.OnEnter(tadpole);
        }

        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Tadpole entered new growth state");
            tadpole = (Tadpole)gameEntity;
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Tadpole left growth state");
        }

        public override void Update(Entity gameEntity)
        {
            if (behaviourState.GetType() != typeof(HideState))
            {
                List<Lilypad> nearbyLilypads = PondEnvironment.Instance.GetNearbyLilypads(tadpole.transform.position, tadpole.SightDistance, true);
                if (nearbyLilypads.Count > 0)
                {
                    List<Entity> entityLilypadList = new List<Entity>(nearbyLilypads.Count);
                    foreach (Lilypad lilypad in nearbyLilypads)
                    {
                        entityLilypadList.Add(lilypad);
                    }

                    while (tadpole.TargetLilypad == null && entityLilypadList.Count > 0) //loop to find closest lilypad that isn't taken
                    {
                        tadpole.TargetLilypad = (Lilypad)PondEnvironment.Instance.GetClosestEntityInList(entityLilypadList, tadpole.transform.position);

                        if (((Tadpole)gameEntity).State.GetType() != typeof(GrowthStateFour))
                        {
                            if (tadpole.TargetLilypad.TadpoleSlot == false)
                            {
                                entityLilypadList.Remove(tadpole.TargetLilypad);
                                tadpole.TargetLilypad = null;
                            }
                        }
                        else
                        {
                            if (tadpole.TargetLilypad.DragonflySlot == false)
                            {
                                entityLilypadList.Remove(tadpole.TargetLilypad);
                                tadpole.TargetLilypad = null;
                            }
                            else
                            {
                                tadpole.TargetLilypad.DragonflySlot = false;
                            }
                        }
                    }
                }
            }

            if (tadpole.State.GetType() != typeof(FleeState) && tadpole.State.GetType() != typeof(GrowthStateFour))
            {
                List<Dragonfly> nearbyDrgonflies = PondEnvironment.Instance.GetNearbyDragonflies(tadpole.transform.position, tadpole.SightDistance);
                if (nearbyDrgonflies.Count > 0)
                {
                    List<Entity> entityDragonflyList = new List<Entity>(nearbyDrgonflies.Count);
                    foreach (Dragonfly dragonfly in nearbyDrgonflies)
                    {
                        entityDragonflyList.Add(dragonfly);
                    }
                    tadpole.Enemy = (Dragonfly)PondEnvironment.Instance.GetClosestEntityInList(entityDragonflyList, tadpole.transform.position);
                }
            }

            if (tadpole.State.GetType() != typeof(FleeState) && tadpole.State.GetType() != typeof(MoveToFoodState) && tadpole.State.GetType() != typeof(GrowthStateFour))
            {
                List<Food> nearbyFood = PondEnvironment.Instance.GetNearbyFood(tadpole.transform.position, tadpole.SightDistance);
                if (nearbyFood.Count > 0)
                {
                    List<Entity> entityFoodList = new List<Entity>(nearbyFood.Count);
                    foreach (Food foodItem in nearbyFood)
                    {
                        entityFoodList.Add(foodItem);
                    }
                    //Debug.Log("Saw food, Setting food ref");
                    if (tadpole.TargetFood != null)
                    {
                        tadpole.TargetFood.Claimed = false;
                    }
                    else
                    {
                        tadpole.TargetFood = (Food)PondEnvironment.Instance.GetClosestEntityInList(entityFoodList, tadpole.transform.position);
                        tadpole.TargetFood.Claimed = true;
                    }
                }
            }
            tadpole.SampleTemparature();
            
            if (behaviourState != null)
            {
                behaviourState.Update(tadpole);
            }  

            //When tadpole fully fed, not scared and within 2.0 of ideal temperature....
            if (tadpole.HungerLevel < 0.1f && tadpole.FearLevel == 0 && (Math.Abs(tadpole.Temperature - tadpole.IdealTemperature) < 3.0f)) //uint cast gaurentees the number is positive
            {
                tadpole.ReadyToEvolve = true;
            }            
        }
    }
}
