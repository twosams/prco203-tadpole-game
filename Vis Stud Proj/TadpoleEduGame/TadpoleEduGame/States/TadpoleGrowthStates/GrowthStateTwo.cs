﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.States.TadpoleBehaviourStates;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleGrowthStates
{
    [Serializable]
    
    class GrowthStateTwo : GrowthState
    {
        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            try
            {
                tadpole = (Tadpole)gameEntity;
                
                SpriteRenderer renderer = tadpole.gameObject.GetComponent<SpriteRenderer>();
                renderer.sprite = Resources.Load<Sprite>("Sprites/Tadpole Stage 2"); //look like a stage 2 tadpole
                tadpole.Speed *= 1.2f; //move faster - you've got legs!
                tadpole.ChangeState(new WanderState());
            }
            catch (Exception ex)
            {
                Console.WriteLine("GrowthStateTwo Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            Console.WriteLine("Tadpole left stage two of growth");
        }

        public override void Update(Entity gameEntity)
        {
            base.Update(gameEntity);

        }
    }
}
