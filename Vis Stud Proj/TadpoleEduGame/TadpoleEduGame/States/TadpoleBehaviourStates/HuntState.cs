﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using TadpoleEduGame.States.TadpoleBehaviourStates;
using TadpoleEduGame.States.TadpoleGrowthStates;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleBehaviourStates
{
    class HuntState : TadpoleState
    {
        private SpriteRenderer spriteRend;
        private Vector3 lineEndPoint;
        private LineRenderer frogLineRend;
        private Vector3 defaultTongueStart;

        public override void OnEnter(Objects.Entity gameEntity)
        {
            Tadpole frog = (Tadpole)gameEntity;
            defaultTongueStart = frog.transform.position;
            if(frog.GetComponent<LineRenderer>() == null)
            {
                frogLineRend = frog.gameObject.AddComponent<LineRenderer>();
            }            
            frogLineRend.SetColors(Color.red, Color.red);
            spriteRend = frog.GetComponent<SpriteRenderer>();
            spriteRend.sortingLayerName = "OnLilypad";
            
            lineEndPoint = frog.transform.position;
            if(frog.TargetLilypad != null)
            {
                Debug.Log("I see lilypads");
                frog.transform.parent = frog.TargetLilypad.transform;
            }
            else
            {
                Debug.Log("No lilypads, back to wander");
                frog.ChangeState(new WanderState());
            }
        }

        public override void OnExit(Objects.Entity gameEntity)
        {
            Debug.Log("Leaving OnExit");
            Tadpole frog = (Tadpole)gameEntity;
            frog.Enemy = null;
            frog.TargetLilypad.DragonflySlot = true;
            frog.TargetLilypad.TadpoleSlot = true;
            frog.TargetLilypad = null;
            frog.transform.parent = null;
            spriteRend.sortingLayerName = "Default";
            GameObject.Destroy(frog.GetComponent<LineRenderer>());
        }

        public override void Update(Objects.Entity gameEntity)
        {
            Tadpole frog = (Tadpole)gameEntity;
            frog.transform.localPosition = Vector3.zero;
            if (frog.TargetLilypad != null)
            {
                if (frog.TargetLilypad.State.GetType() == typeof(TadpoleEduGame.States.LilypadStates.RottedState) && frog.State.GetType() == typeof(GrowthStateFour))
                {
                    frog.ChangeState(new WanderState());
                }
                if (frog.Enemy != null) //seen a dragonfly
                {
                    //Line renderer start point = frog position
                    frogLineRend.SetPosition(0, frog.transform.position);

                    frog.LookAt(frog.Enemy.transform.position);
                    float distanceToDragonfly = Vector3.Distance(frog.Enemy.transform.position, frog.transform.position);

                    if (distanceToDragonfly > 10) //out of range, give up
                    {
                        frog.ChangeState(new WanderState());
                    }
                    else //still worth trying
                    {
                        if (distanceToDragonfly < 8)
                        {
                            frog.MoveTongue();
                            lineEndPoint = frog.TongueEndLocation;
                            frogLineRend.SetPosition(1, lineEndPoint);
                        }
                        else
                        {
                            lineEndPoint = frog.transform.position;
                            frogLineRend.SetPosition(1, lineEndPoint);
                        }
                    }
                }
                else
                {
                    frogLineRend.SetPosition(0, frog.transform.position);
                    frogLineRend.SetPosition(0, frog.transform.position + frog.transform.forward * 2.5f);
                    frogLineRend.SetPosition(1, frog.transform.position);
                    frogLineRend.SetPosition(1, frog.transform.position + frog.transform.forward * 5f);

                    List<Dragonfly> nearbyDragonflies = PondEnvironment.Instance.GetNearbyDragonflies(frog.transform.position, 8.0f);

                    if (nearbyDragonflies.Count > 0)
                    {
                        System.Random rand = new System.Random();
                        int cho = rand.Next(0, nearbyDragonflies.Count);
                        frog.Enemy = nearbyDragonflies[cho];
                    }
                }
            }
            else
            {
                frog.ChangeState(new WanderState());
            }
            
        }


        
    }
}
