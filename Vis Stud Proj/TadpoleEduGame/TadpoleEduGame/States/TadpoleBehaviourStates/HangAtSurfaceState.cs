﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleBehaviourStates
{
    [Serializable]
    /// <summary>
    /// The state a tadpole is in if it 
    /// </summary>
    class HangAtSurfaceState : TadpoleState
    {
        Sprite originalSprite;
        SpriteRenderer renderer;

        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Tadpole entered hanging state");
            renderer = gameEntity.gameObject.GetComponent<SpriteRenderer>();
            originalSprite = renderer.sprite;
            renderer.sprite = Resources.Load<Sprite>("Sprites/Tadpole Up");

            //gameEntity.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Tadpole Up");

            //Tadpole tadpole = (Tadpole)gameEntity;
            //RaycastHit rayHit;
            //Physics.Raycast(tadpole.transform.position, Vector3.up, out rayHit, 10.0f);
            //WaterSurface hitSurface = rayHit.collider.GetComponent<WaterSurface>();
            //if (hitSurface == null) //Not near the surface - we're incorrectly in this state
            //{
            //    tadpole.ChangeState(new MoveToSurfaceState());
            //}
            //else
            //{
            //    //change sprite and don't really move
            //}
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Tadpole exited hanging state");
            renderer.sprite = originalSprite;
        }

        public override void Update(Entity gameEntity)
        {
            Tadpole tadpole = (Tadpole)gameEntity;

            if (tadpole.Enemy != null || tadpole.KnownDisturbance != Vector3.zero)
            {
                tadpole.ChangeState(new FleeState());
            }
            else
            {
                System.Random random = new System.Random(Guid.NewGuid().GetHashCode());
                int randNo = random.Next(1000);
                if (randNo > 997)//0.5% chance to leave the surface (every frame - may need changing)
                {
                    tadpole.ChangeState(new WanderState());
                }
            }
        }
    }
}
