﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;
using TadpoleEduGame.States.TadpoleGrowthStates;

namespace TadpoleEduGame.States.TadpoleBehaviourStates
{
    [Serializable]
    class MoveToLilypadState : TadpoleState
    {
        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Tadpole moving to shelter under lilypad");
            ((Tadpole)gameEntity).TargetLilypad.TadpoleSlot = false;
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Tadpole leaving lilypad");
            if(((Tadpole)gameEntity).TargetLilypad != null)
            {
                ((Tadpole)gameEntity).TargetLilypad.TadpoleSlot = false;
            }            
        }

        public override void Update(Entity gameEntity)
        {
            Tadpole tadpole = (Tadpole)gameEntity;
            if (tadpole.TargetLilypad != null)
            {
                Vector3 targetPos = new Vector3(tadpole.TargetLilypad.transform.position.x, tadpole.TargetLilypad.transform.position.y, tadpole.TargetLilypad.transform.position.z + 0.02f);
                Debug.LogWarning("MoveToLilypad Update");


                if (tadpole.TargetFood != null)
                {
                    tadpole.ChangeState(new MoveToFoodState());
                }

                if (tadpole.transform.position != targetPos)
                {
                    tadpole.HeadTowards(targetPos);
                }
                else
                {
                    if (tadpole.State.GetType() == typeof(GrowthStateFour))
                    {
                        Debug.LogWarning("About to enter huntState");
                        tadpole.ChangeState(new HuntState());
                    }
                    else
                    {
                        tadpole.ChangeState(new HideState());
                    }
                }
                if(tadpole.TargetLilypad.State.GetType()== typeof(TadpoleEduGame.States.LilypadStates.RottedState) && tadpole.State.GetType() == typeof(GrowthStateFour))
                {
                    tadpole.ChangeState(new WanderState());
                }
            }
            else
            {
                tadpole.ChangeState(new WanderState());
            }

                        
        }
    }
}
