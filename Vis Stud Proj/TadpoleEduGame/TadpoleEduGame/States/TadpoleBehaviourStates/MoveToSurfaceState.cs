﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleBehaviourStates
{
    [Serializable]
    class MoveToSurfaceState : TadpoleState
    {
        Vector3 targetPoint;

        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Tadpole moving to surface");
            Tadpole tadpole = (Tadpole)gameEntity;
            RaycastHit rayHit;
            Physics.Raycast(tadpole.transform.position, Vector3.back, out rayHit, 30.0f);
            if (rayHit.collider != null)
            {
                if (rayHit.collider.GetComponent<WaterSurface>() != null)
                {
                    targetPoint = rayHit.point;
                }
                else
                {
                    tadpole.ChangeState(new WanderState());
                }
            }
            else
            {
                tadpole.HeadTowards(tadpole.transform.position + Vector3.up);
                tadpole.ChangeState(new WanderState());
            }
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Tadpole leaving surface");
        }

        public override void Update(Entity gameEntity)
        {
            Tadpole tadpole = (Tadpole)gameEntity;
            if (tadpole.transform.position != targetPoint)
            {
                tadpole.HeadTowards(targetPoint);
            }
            else //at the surface
            {
                tadpole.ChangeState(new HangAtSurfaceState());
            }
        }
    }
}
