﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;
using TadpoleEduGame.Utility;

namespace TadpoleEduGame.States.TadpoleBehaviourStates
{
    [Serializable]
    /// <summary>
    /// The state a tadpole is in if it notices a dragonfly
    /// </summary>
    class FleeState : TadpoleState
    {
        Vector3 oppositeDirectionFromEnemy;

        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Tadpole entered flee state");
            ((Tadpole)gameEntity).Speed *= 3f;
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Tadpole left flee state");
            ((Tadpole)gameEntity).Speed /= 3f;
            ((Tadpole)gameEntity).Enemy = null;
        }

        public override void Update(Entity gameEntity)
        {
            Tadpole tadpole = (Tadpole)gameEntity;
            if (tadpole.Enemy == null && tadpole.KnownDisturbance == Vector3.zero)
            {
                tadpole.ChangeState(new WanderState());
                ////Debug.Log("Wandering");
            }
            else
            {
                ////Debug.Log("Scary things");
                if (tadpole.Enemy != null)
                {
                    oppositeDirectionFromEnemy = PositionUtility.OppositeDirectionTo(tadpole.transform.position, tadpole.Enemy.transform.position);
                    oppositeDirectionFromEnemy.z = tadpole.transform.position.z;
                    if (Vector3.Distance(tadpole.transform.position, oppositeDirectionFromEnemy) > 8.0f)
                    {
                        tadpole.Enemy = null;
                        tadpole.ChangeState(new WanderState());
                    }
                }

                if (tadpole.KnownDisturbance != Vector3.zero)
                {
                    oppositeDirectionFromEnemy = PositionUtility.OppositeDirectionTo(tadpole.transform.position, tadpole.KnownDisturbance);
                    oppositeDirectionFromEnemy.z = tadpole.transform.position.z;
                    if (Vector3.Distance(tadpole.transform.position, oppositeDirectionFromEnemy) > 8.0f)
                    {
                        tadpole.KnownDisturbance = Vector3.zero;
                        tadpole.ChangeState(new WanderState());
                        
                    }
                }
                //Debug.LogWarning("Flee update");


                tadpole.HeadTowards(oppositeDirectionFromEnemy);

            }
        }
    }
}
