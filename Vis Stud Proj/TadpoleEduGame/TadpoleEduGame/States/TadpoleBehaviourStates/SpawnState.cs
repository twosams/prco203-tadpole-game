﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using TadpoleEduGame.Objects;
using TadpoleEduGame.States.TadpoleGrowthStates;

namespace TadpoleEduGame.States.TadpoleBehaviourStates
{
    class SpawnState : TadpoleState
    {
        private float timer;

        public override void OnEnter(Objects.Entity gameEntity)
        {            
            System.Random rand = new System.Random(Guid.NewGuid().GetHashCode());
            Tadpole tadpole = (Tadpole)gameEntity;

            timer = 3 + (rand.Next(0,50) / 10);
        }

        public override void OnExit(Objects.Entity gameEntity)
        {
            //Debug.Log("Tapdole spawned");
        }

        public override void Update(Objects.Entity gameEntity)
        {
            timer -= Time.deltaTime;

            if (timer < 0)
            {
                ((Tadpole)gameEntity).ChangeState(new GrowthStateOne());
            }
        }
    }
}
