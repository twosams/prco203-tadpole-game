﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleBehaviourStates
{
    [Serializable]
    /// <summary>
    /// The state a tadpole is in if it is underneath a lilypad
    /// </summary>
    class HideState : TadpoleState
    {
        Vector3 targetPoint;

        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Tadpole entered hide state");
            //((Tadpole)gameEntity).TargetLilypad.ChangeTransparency(0.30f);
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Tadpole exited hide state");
            
            ((Tadpole)gameEntity).TargetLilypad = null;
        }

        public override void Update(Entity gameEntity)
        {
            Tadpole tadpole = (Tadpole)gameEntity;
            System.Random random = new System.Random();
            int choice = random.Next(1000);

            if (targetPoint != null && tadpole.transform.position != targetPoint)
            {
                tadpole.HeadTowards(targetPoint);
            }

            if (tadpole.TargetFood != null) //should the tadpole be smart enough to ignore food if there is a dragonfly about? If so, add "&& tadpole.Enemy == null"
            {
                tadpole.ChangeState(new MoveToFoodState());
            }
            else if(choice > 970 && tadpole.Enemy == null) //only leave hiding if there's no dragonflies about
            {
                tadpole.ChangeState(new WanderState());
            }
            else
            {
                RaycastHit rayHit;
                System.Random rand = new System.Random();

                int deviation = 3;

                Vector3 newPos = tadpole.transform.position;

                int randomNum = rand.Next(0,100);
                int randomNum2 = rand.Next(0,100);

                if(randomNum <= 50)
                {
                    randomNum = rand.Next(0, deviation);
                }
                else
                {
                    randomNum = rand.Next(0, deviation);
                    randomNum = 0 - randomNum;
                }

                if (randomNum2 <= 50)
                {
                    randomNum2 = rand.Next(0, deviation);
                }
                else
                {
                    randomNum2 = rand.Next(0, deviation);
                    randomNum2 = 0 - randomNum;
                }

                newPos.x += randomNum;
                newPos.y += randomNum2;

                Physics.Raycast(newPos, Vector3.back, out rayHit, 30.0f);
                if (rayHit.collider != null)
                {
                    if (rayHit.collider.GetComponent<Lilypad>() != null)
                    {
                        targetPoint = newPos; //assign the target position
                    }
                }
            }
        }
    }
}
