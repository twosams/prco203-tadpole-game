﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleBehaviourStates
{
    [Serializable]
    public abstract class TadpoleState : IState
    {
        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public abstract void OnEnter(Entity gameEntity);

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public abstract void OnExit(Entity gameEntity);

        public abstract void Update(Entity gameEntity);
    }
}
