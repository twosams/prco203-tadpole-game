﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;
using TadpoleEduGame.Utility;
using TadpoleEduGame.States.TadpoleGrowthStates;

namespace TadpoleEduGame.States.TadpoleBehaviourStates
{
    [Serializable]
    /// <summary>
    /// The base state - the tadpole wanders randomly around the area
    /// </summary>
    class WanderState : TadpoleState
    {
        private Vector3 targetPos = Vector3.up;

        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Tadpole started wandering");
            targetPos = NewRandomTarget();
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Tadpole stopped wandering.");
        }

        public override void Update(Entity gameEntity)
        {
            Tadpole tadpole = (Tadpole)gameEntity;

            if ((tadpole.Enemy != null || tadpole.KnownDisturbance != Vector3.zero) && !(tadpole.State.GetType() == typeof(GrowthStateFour))) //If a dragonfly has been spotted
            {                
                tadpole.ChangeState(new FleeState());
            }
            else if (tadpole.TargetFood != null)//If we've spotted some food
            {
                tadpole.ChangeState(new MoveToFoodState());
            }
            else if (tadpole.TargetLilypad != null && !(tadpole.State.GetType() == typeof(GrowthStateFour) && tadpole.TargetLilypad.State.GetType() == typeof(TadpoleEduGame.States.LilypadStates.RottedState))) //If a lilypad has been spotted
            {                
                tadpole.ChangeState(new MoveToLilypadState());
            }
            else if (tadpole.transform.position != targetPos) //If we're not at our destination and haven't spotted anything
            {
                RaycastHit rayHit;
                Physics.Raycast(tadpole.transform.position, tadpole.transform.rotation.eulerAngles.normalized, out rayHit, 5.0f);

                if (rayHit.collider != null && rayHit.collider.name == "Pond") //at the edge of the pond
                {
                    targetPos = NewRandomTarget();
                }
                

                tadpole.HeadTowards(targetPos);
            }
            else //we've arrived and not spotted anything
            {
                System.Random random = new System.Random();
                int chance = random.Next(1000);
                if (chance > 990)//1% chance to keep moving randomly, 99% chance to go and hang at the surface
                {
                    targetPos = NewRandomTarget();
                }
                else
                {
                    tadpole.ChangeState(new MoveToSurfaceState());
                }

            }

        }

        private Vector3 NewRandomTarget()
        {
            Vector3 target = PositionUtility.NewRandomTarget();

            //Limit the locations to areas inside the pond
            target = PositionUtility.LimitPositionTo(32.0f, 21.0f, -5.0f, -31.0f, -17.0f, -Mathf.Infinity, target);
            return target;
        }
    }
}
