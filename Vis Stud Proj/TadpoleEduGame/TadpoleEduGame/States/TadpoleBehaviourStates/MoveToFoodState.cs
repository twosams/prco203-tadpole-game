﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.TadpoleBehaviourStates
{
    [Serializable]
    class MoveToFoodState : TadpoleState
    {
        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Tadpole moving to food");  
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            ((Tadpole)gameEntity).TargetFood = null;
            //Debug.Log("Tadpole not moving to food any more");
        }

        public override void Update(Entity gameEntity)
        {
            Tadpole tadpole = (Tadpole)gameEntity;
            if (tadpole.TargetFood != null) //food hasn't been eaten by anyone else
            {
                if (tadpole.transform.position != tadpole.TargetFood.transform.position)//not at food
                {
                    tadpole.HeadTowards(tadpole.TargetFood.transform.position);
                }
                else//at food
                {
                    tadpole.TargetFood.BeEaten(tadpole);
                    tadpole.ChangeState(new WanderState());
                }
            }
            else //someone else beat us to the food and ate it
            {
                tadpole.ChangeState(new WanderState());
            }
        }
    }
}
