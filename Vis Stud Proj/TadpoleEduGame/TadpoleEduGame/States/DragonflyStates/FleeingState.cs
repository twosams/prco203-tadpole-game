﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;
using TadpoleEduGame.Interfaces;

namespace TadpoleEduGame.States.DragonflyStates
{
    [Serializable]

    class FleeingState : DragonflyState
    {
        Renderer dragRend;

        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Dragonfly is Fleeing");
            dragRend = ((Dragonfly)gameEntity).GetComponent<Renderer>();
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Dragonfly has been destroyed");
        }

        public override void Update(Entity gameEntity)
        {
            Dragonfly dragonfly = (Dragonfly)gameEntity;

            dragonfly.HeadTowards(new Vector3(130, 0, -16));

            if (dragRend.isVisible != true)
            {
                GameObject.Destroy(dragonfly.gameObject);
            }
        }

    }


}
