﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.DragonflyStates
{
    [Serializable]
    class LandState : DragonflyState
    {
        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Dragonfly moving to land");
            ((Dragonfly)gameEntity).TargetLilypad.DragonflySlot = false;
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Dragonfly no longer moving to land");
            ((Dragonfly)gameEntity).TargetLilypad.DragonflySlot = true;
        }

        public override void Update(Entity gameEntity)
        {
            Dragonfly dragonfly = (Dragonfly)gameEntity;
            if (dragonfly.TargetLilypad != null)
            {
                if (dragonfly.transform.position != dragonfly.TargetLilypad.transform.position)
                {
                    //move to lilypad & land
                    dragonfly.HeadTowards(dragonfly.TargetLilypad.transform.position);
                }
                else
                {
                    dragonfly.ChangeState(new LandedState());
                }
            }
            else //no longer have a lilypad to land on
            {
                dragonfly.ChangeState(new FlyingState());
            }
        }
    }
}
