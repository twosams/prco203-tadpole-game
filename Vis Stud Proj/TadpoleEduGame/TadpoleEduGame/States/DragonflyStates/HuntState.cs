﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.DragonflyStates
{
    [Serializable]
    class HuntState : DragonflyState
    {
        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Dragonfly is hunting");
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Dragonfly no longer hunting");
            ((Dragonfly)gameEntity).TargetLilypad = null; //how to make it not immedeately land again?
        }

        public override void Update(Entity gameEntity)
        {
            Dragonfly dragonfly = (Dragonfly)gameEntity;
            if (dragonfly.TargetTadpole != null && dragonfly.TimeSinceLastAte > dragonfly.MinimumEatTime)
            {
                if (Vector3.Distance(dragonfly.transform.position, dragonfly.TargetTadpole.transform.position) > 3.0f)
                {
                    dragonfly.HeadTowards(dragonfly.TargetTadpole.transform.position);
                }
                else
                {
                    dragonfly.TargetTadpole.BeEaten(dragonfly); //timeSinceLastAte is set to zero in here
                    dragonfly.ChangeState(new FlyingState());
                }
            }
            else //no target
            {
                dragonfly.ChangeState(new FlyingState());
            }
        }
    }
}
