﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using TadpoleEduGame.Utility;
using UnityEngine;

namespace TadpoleEduGame.States.DragonflyStates
{
    [Serializable]
    class FlyingState : DragonflyState
    {
        Vector3 targetPos;

        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Dragonfly flying");
            NewTargetPos();
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Dragonfly no longer flying");
        }

        public override void Update(Entity gameEntity)
        {
            Dragonfly dragonfly = (Dragonfly)gameEntity;     

            if (dragonfly.TargetTadpole != null && dragonfly.TimeSinceLastAte > dragonfly.MinimumEatTime)
            {
                dragonfly.ChangeState(new HuntState());
            }
            else if (dragonfly.TargetLilypad != null)
            {
                System.Random random = new System.Random();
                int choice = random.Next(1000);
                if (choice > 950)//5% chance of going to a spotted lilypad
                {
                    dragonfly.ChangeState(new LandState());
                }
            }
            else 
            {
                if (dragonfly.transform.position != targetPos)
                {
                    dragonfly.HeadTowards(targetPos);
                }
                else
                {
                    NewTargetPos();
                }
            }
        }

        private void NewTargetPos()
        {
            targetPos = PositionUtility.NewRandomTarget();
            targetPos.z = -14;
        }
    }
}
