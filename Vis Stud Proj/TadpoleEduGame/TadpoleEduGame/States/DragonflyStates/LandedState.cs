﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.States.DragonflyStates
{
    [Serializable]
    class LandedState : DragonflyState
    {
        /// <summary>
        /// Called when this state is first entered. Any setup for the state is done here
        /// </summary>
        /// <param name="gameEntity">The entity entering this state</param>
        public override void OnEnter(Entity gameEntity)
        {
            //Debug.Log("Dragonfly has landed");
            Dragonfly dragonfly = (Dragonfly)gameEntity;
            dragonfly.transform.parent = dragonfly.TargetLilypad.transform;
        }

        /// <summary>
        /// Called when the state is being left. Any cleanup for the state is done here
        /// </summary>
        /// <param name="gameEntity">the entity leaving this state</param>
        public override void OnExit(Entity gameEntity)
        {
            //Debug.Log("Dragonfly leaving landed state");
            Dragonfly dragonfly = (Dragonfly)gameEntity;
            dragonfly.TargetLilypad = null;
            dragonfly.transform.parent = null;
        }

        public override void Update(Entity gameEntity)
        {
            Debug.LogWarning("1");
            Dragonfly dragonfly = (Dragonfly)gameEntity;
            Debug.LogWarning("2");
            dragonfly.transform.localPosition = Vector3.zero;
            Debug.LogWarning("3");
            System.Random random = new System.Random();
            Debug.LogWarning("4");
            int choice = random.Next(1000);
            if (choice > 970) //5% chance of leaving lilypad to fly
            {
                Debug.LogWarning("5");
                (dragonfly).ChangeState(new FlyingState());
            }

            if (dragonfly.TargetTadpole != null && dragonfly.TimeSinceLastAte > 2.0f)
            {
                Debug.LogWarning("6");
                dragonfly.ChangeState(new HuntState());
            }

            if (Input.GetMouseButtonDown(0))
            {
                Debug.LogWarning("7");
                //Debug.Log("Dragonfly noticed mouse click");
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 30))
                {
                    Debug.LogWarning("8");
                    //Debug.Log("Dragonfly raycast sent");
                    //Debug.Log("Hit = " + hit.collider.name);
                    if(hit.transform != null && hit.transform.parent != null)
                    {
                        if (hit.transform.parent.CompareTag("Dragonfly"))
                        {
                            Debug.LogWarning("9");
                            //Debug.Log("Dragonfly running away");
                            dragonfly.ChangeState(new FleeingState());
                        }
                    }                    
                }
                Debug.LogWarning("10");

            }
        }
    }
}
