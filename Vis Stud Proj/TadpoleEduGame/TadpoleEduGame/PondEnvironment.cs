﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;
using TadpoleEduGame.States.TadpoleBehaviourStates;
using TadpoleEduGame.States.LilypadStates;
using TadpoleEduGame.Interfaces;
using TadpoleEduGame.States.TadpoleGrowthStates;
using TadpoleEduGame.Utility;

namespace TadpoleEduGame
{
    /// <summary>
    /// the parent object that manages the entire pond ecosystem
    /// </summary>
    public sealed class PondEnvironment : IUpdatable
    {
        private List<Tadpole> tadpoles;
        private List<Lilypad> lilypads;
        private List<Food> food;
        private List<Dragonfly> dragonflies;
        private WaterSurface waterSurface;

        GameObject mapParent;
        

        private int maxXTempMap;
        private int maxYTempMap;
        private int[,] temperatureMap;
        private GameObject[,] temperatureMapObjects;
        private static readonly PondEnvironment instance = new PondEnvironment();

        /// <summary>
        /// Get the singular instance of this object
        /// </summary>
        public static PondEnvironment Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Create a new pond environment to hold tadpoles, dragonflies, lilypads and food
        /// </summary>
        private PondEnvironment()
        {
            tadpoles = new List<Tadpole>();
            lilypads = new List<Lilypad>();
            food = new List<Food>();
            dragonflies = new List<Dragonfly>();
            maxXTempMap = 8;
            maxYTempMap = 8;
            temperatureMap = new int[maxXTempMap, maxYTempMap];
        }

        /// <summary>
        /// Loads lists of objects for the system, discards any current lists in favour of these
        /// </summary>
        /// <param name="tadpoleList"></param>
        /// <param name="dragonflyList"></param>
        /// <param name="lilypadList"></param>
        /// <param name="waterSurface"></param>
        public void LoadItemLists(List<Tadpole> tadpoleList, List<Dragonfly> dragonflyList, List<Lilypad> lilypadList, WaterSurface waterSurface)
        {
            tadpoles = tadpoleList;
            dragonflies = dragonflyList;
            lilypads = lilypadList;
            this.waterSurface = waterSurface;
        }        

        /// <summary>
        /// Replaces the current waterSurface
        /// </summary>
        /// <param name="waterSurface"></param>
        public void LoadWaterSurface(WaterSurface waterSurface)
        {
            this.waterSurface = waterSurface;
        }

        /// <summary>
        /// Replaces the current lilypads with those in the given list
        /// </summary>
        /// <param name="lilypadList">Lilypads to use</param>
        public void LoadLilypadList(List<Lilypad> lilypadList)
        {
            lilypads = lilypadList;
        }

        /// <summary>
        /// Replaces the current tadpoles with those in the given list
        /// </summary>
        /// <param name="tadpoleList">Tadpoles to load</param>
        public void LoadTadpoleList(List<Tadpole> tadpoleList)
        {
            tadpoles = tadpoleList;
        }

        /// <summary>
        /// Replaces the current dragonflies with those in the given list
        /// </summary>
        /// <param name="dragonflyList">Dragonflies to load</param>
        public void LoadDragonflyList(List<Dragonfly> dragonflyList)
        {
            dragonflies = dragonflyList;
        }

        /// <summary>
        /// Generate the array of temparatures
        /// </summary>
        public void GenerateTemperatures(GameObject mapObject, Vector3 location)
        {
            float avrSurround = 0;
            int startVal = 23;
            int invalidVal = -999;
            temperatureMapObjects = new GameObject[maxXTempMap, maxYTempMap];
            mapParent = new GameObject("_TemperatureMap");
            mapParent.transform.position = location;

            System.Random rand = new System.Random();

            for (int outerCount = 0; outerCount < maxXTempMap; outerCount++)
            {
                for (int innerCount = 0; innerCount < maxYTempMap; innerCount++)
                {
                    temperatureMap[innerCount, outerCount] = rand.Next(startVal -8, startVal +9); //assign a variety of random values to start
                }
            }

            Renderer renderer; //allocate once outside the loops, more efficient
            for (int pass = 0; pass < 3; pass++) //more passes = less diversity and slower generation, too few = large gradient between squares but faster generation(unrealistic)
            {
                for (int outerCount = 0; outerCount < maxXTempMap; outerCount++) //effectively the x value of the object
                {
                    for (int innerCount = 0; innerCount < maxYTempMap; innerCount++)
                    {
                            avrSurround = GetAverageSurroundingTemeratures(outerCount, innerCount, invalidVal); //Get the average temperatures of the surrounding squares
                            int value = rand.Next((int)(avrSurround - 2), (int)(avrSurround + 3)); //using the average set our own temperature near that range
                            temperatureMap[outerCount, innerCount] = value;

                        if (pass == 0) //Only on the first pass...
                        {
                            temperatureMapObjects[innerCount, outerCount] = GameObject.Instantiate(mapObject); //create the objects
                        }

                        renderer = temperatureMapObjects[innerCount, outerCount].GetComponent<Renderer>(); //get a reference to this squares renderer.
                        renderer.material = new Material(renderer.material);
                        Color color = renderer.material.color;
                        renderer.material.SetColor("_Color", new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, (float)PositionUtility.Sigmoid(temperatureMap[innerCount, outerCount]/startVal))); //Set the alpha using the sigmoid function to keep between 0-1
                        float xVal = outerCount * temperatureMapObjects[innerCount, outerCount].transform.localScale.x * 10;
                        float yVal = innerCount * temperatureMapObjects[innerCount, outerCount].transform.localScale.y * 10;
                        float zVal = -10.1f; //Lock the height of the map
                        temperatureMapObjects[innerCount, outerCount].transform.position = new Vector3(xVal, yVal, zVal) +  (mapParent.transform.position);// - new Vector3(waterSurface.transform.localScale.x * 5, waterSurface.transform.localScale.x * 5));
                        temperatureMapObjects[innerCount, outerCount].transform.parent = mapParent.transform;
                        ////Debug.Log("[" + outerCount + "," + innerCount + "] - " + temperatureMap[outerCount, innerCount] + "(Average surrounding was " + avrSurround + ")");
                    }
                }
            }
            HideTemperatureMap(); //Don't need the visual element showing, at least initially
        }

        /// <summary>
        /// Helper method to get the average temperature of surrounding temperature 'squares'
        /// </summary>
        /// <param name="x">X value</param>
        /// <param name="y">Y value</param>
        private float GetAverageSurroundingTemeratures(int x, int y, int invalidVal)
        {
            float total = 0;
            float average;
            int count = 0;

            if (x != 0) //left
            {
                if (temperatureMap[x - 1, y] != invalidVal)
                {
                    total += temperatureMap[x - 1, y];
                    count++;
                }
            }

            if(x != maxXTempMap-1) //right
            {
                if (temperatureMap[x + 1, y] != invalidVal)
                {
                    total += temperatureMap[x + 1, y];
                    count++;
                }
            }

            if (y != 0) //below
            {
                if (temperatureMap[x, y - 1] != invalidVal)
                {
                    total += temperatureMap[x, y - 1];
                    count++;
                }
            }

            if (y != maxYTempMap - 1) //above
            {
                if (temperatureMap[x, y + 1] != invalidVal)
                {
                    total += temperatureMap[x, y + 1];
                    count++;
                }
            }

            average = total / count;

            return average;
        }

        /// <summary>
        /// Gets the temerature from the temperature map for a given location
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public int GetTemperatureAt(Vector3 location)
        {
            int temperature = 0;
            GameObject testObj = new GameObject();

            testObj.transform.position = location;
            testObj.transform.parent = mapParent.transform;
            location = testObj.transform.localPosition;

            location.x = Mathf.Round(location.x / 10) * 10;
            location.y = Mathf.Round(location.y / 10) * 10;
            location.z = -10.1f;

            for (int outerCount = 0; outerCount < maxXTempMap; outerCount++)
            {
                for (int innerCount = 0; innerCount < maxYTempMap; innerCount++)
                {
                    float xVal = outerCount * temperatureMapObjects[innerCount, outerCount].transform.localScale.x * 10;
                    float yVal = innerCount * temperatureMapObjects[innerCount, outerCount].transform.localScale.y * 10;
                    float zVal = -10.1f;

                    testObj.transform.position = new Vector3(xVal, yVal, zVal) + (mapParent.transform.position);
                    testObj.transform.parent = mapParent.transform;

                    if (location == testObj.transform.localPosition)
                    {
                        temperature = temperatureMap[innerCount, outerCount];
                        break;
                    }
                }
            }
            GameObject.Destroy(testObj);
            return temperature;
        }

        /// <summary>
        /// The given dragonfly is removed from the system
        /// </summary>
        /// <param name="dragonfly">Dragonfly to be removed</param>
        public void RemoveDragonFly(Dragonfly dragonfly)
        {
            foreach (Tadpole tadpole in tadpoles)
            {
                if (tadpole.Enemy == dragonfly)
                {
                    tadpole.Enemy = null;
                }
            }

            dragonflies.Remove(dragonfly);
        }

        private void RemoveTadpole(Tadpole tadpole)
        {
            tadpoles.Remove(tadpole);
        }

        /// <summary>
        /// Returns food within a radius of a given location
        /// </summary>
        /// <param name="location">The point from which to find food</param>
        /// <param name="thresholdDistance">The furthest distance from the point the food can be</param>
        /// <returns>The food within the radius from the given point</returns>
        public List<Food> GetNearbyFood(Vector3 location, float thresholdDistance)
        {
            RemoveNullItemsFromLists();
            List<Food> nearbyFood = new List<Food>();
            foreach (Food foodItem in food)
            {
                if (foodItem != null)
                {
                    if (Vector3.Distance(location, foodItem.transform.position) <= thresholdDistance)
                    {
                        //Debug.Log("Edible food seen!");
                        if (foodItem.Claimed == false)
                        {
                            nearbyFood.Add(foodItem);
                        }
                    }
                }
            }

            return nearbyFood;
        }

        /// <summary>
        /// Returns dragonflies within a radius of a given location
        /// </summary>
        /// <param name="location">The point from which to find dragonflies</param>
        /// <param name="thresholdDistance">The furthest distance from the point the dragonflies can be</param>
        /// <returns>The dragonflies within the radius from the given point</returns>
        public List<Dragonfly> GetNearbyDragonflies(Vector3 location, float thresholdDistance)
        {
            RemoveNullItemsFromLists();
            List<Dragonfly> nearbyDragonfly = new List<Dragonfly>();
            foreach (Dragonfly dragonfly in dragonflies)
            {
                if (Vector3.Distance(location, dragonfly.transform.position) <= thresholdDistance)
                {
                    nearbyDragonfly.Add(dragonfly);
                }
            }

            return nearbyDragonfly;
        }

        /// <summary>
        /// Returns lilypads within a radius of a given location
        /// </summary>
        /// <param name="location">The point from which to find lilypads</param>
        /// <param name="thresholdDistance">The furthest distance from the point the lilypads can be</param>
        /// <returns>The lilypads within the radius from the given point</returns>
        public List<Lilypad> GetNearbyLilypads(Vector3 location, float thresholdDistance, bool onlyHealthy)
        {
            RemoveNullItemsFromLists();
            List<Lilypad> nearbyLilypads = new List<Lilypad>();
            foreach (Lilypad lilypad in lilypads)
            {
                if (Vector3.Distance(location, lilypad.transform.position) <= thresholdDistance)
                {
                    if (onlyHealthy && lilypad.State.GetType() != typeof(RottedState))
                    {
                        nearbyLilypads.Add(lilypad);
                    }
                    else if(!onlyHealthy)
                    {
                        nearbyLilypads.Add(lilypad);
                    }
                }
            }
            return nearbyLilypads;
        }

        /// <summary>
        /// Returns tadpoles within a radius of a given location
        /// </summary>
        /// <param name="location">The point from which to find tadpoles</param>
        /// <param name="thresholdDistance">The furthest distance from the point the tadpoles can be</param>
        /// <returns>The tadpoles within the radius from the given point</returns>
        public List<Tadpole> GetNearbyTadpoles(Vector3 location, float thresholdDistance)
        {
            RemoveNullItemsFromLists();
            List<Tadpole> nearbyTadpoles = new List<Tadpole>();
            foreach (Tadpole tadpole in tadpoles)
            {
                if (Vector3.Distance(location, tadpole.transform.position) <= thresholdDistance)
                {
                    if (tadpole.State.GetType() != typeof(RottedState))
                    {
                        nearbyTadpoles.Add(tadpole);
                    }
                }
            }

            return nearbyTadpoles;
        }

        /// <summary>
        /// Returns the entity closest to a given point from a given list
        /// </summary>
        /// <param name="listEntity">List of entities to check</param>
        /// <param name="currLocation">Point to be closest to</param>
        /// <returns>The closest entity to the point</returns>
        public Entity GetClosestEntityInList(List<Entity> listEntity, Vector3 currLocation)
        {
            RemoveNullItemsFromLists();
            Entity choice = null;
            float closest = Mathf.Infinity;
            float distance;
            foreach (Entity entity in listEntity)
            {
                distance = Vector3.Distance(entity.transform.position, currLocation);
                if (distance < closest)
                {
                    closest = distance;
                    choice = entity;
                }
            }

            return choice;
        }

        /// <summary>
        /// Adds an existing food item into the system
        /// </summary>
        /// <param name="foodItem">The item to be added</param>
        public void AddFoodReference(Food foodItem)
        {
            food.Add(foodItem);
        }

        /// <summary>
        /// Adds an existing lilypad to the system
        /// </summary>
        /// <param name="lilypadItem">Lilypad to be added</param>
        public void AddLilypadReference(Lilypad lilypadItem)
        {            
            lilypads.Add(lilypadItem);
            Debug.Log("New lilypad, count = " + lilypads.Count);
        }

        public void RemoveNullItemsFromLists()
        {
            tadpoles.RemoveAll(item => item == null);
            lilypads.RemoveAll(item => item == null);
            dragonflies.RemoveAll(item => item == null);
        }

        /// <summary>
        /// Update all the items in the environment
        /// </summary>
        public void UpdateItem()
        {
            RemoveNullItemsFromLists();
            foreach (Tadpole tadpole in tadpoles)
            {
                tadpole.UpdateItem();
            }

            foreach (Lilypad lilypad in lilypads)
            {
                lilypad.UpdateItem();
            }

            foreach (Dragonfly dragonfly in dragonflies)
            {
                dragonfly.UpdateItem();
            }
        }        

        /// <summary>
        /// Removes all tadpoles at a given stage
        /// </summary>
        /// <param name="stageNum">The stage of the tadpoles to remove</param>
        public void RemoveTadpolesAtStage(int stageNum)
        {
            List<Tadpole> removeTadpoles = new List<Tadpole>();

            foreach (Tadpole tadpole in tadpoles)
            {   
                if (tadpole.State.GetType() == typeof(GrowthStateZero) && stageNum == 0)
                {
                    removeTadpoles.Add(tadpole);
                    GameObject.Destroy(tadpole.gameObject);
                }

                if (tadpole.State.GetType() == typeof(GrowthStateOne) && stageNum == 1)
                {
                    removeTadpoles.Add(tadpole);
                    GameObject.Destroy(tadpole.gameObject);
                }

                if (tadpole.State.GetType() == typeof(GrowthStateTwo) && stageNum == 2)
                {
                    removeTadpoles.Add(tadpole);
                    GameObject.Destroy(tadpole.gameObject);
                }

                if (tadpole.State.GetType() == typeof(GrowthStateThree) && stageNum == 3)
                {
                    removeTadpoles.Add(tadpole);
                    GameObject.Destroy(tadpole.gameObject);
                }
            }

            for (int count = 0; count < removeTadpoles.Count; count++)
            {
                RemoveTadpole(removeTadpoles[count]);
            }
            RemoveNullItemsFromLists();
        }

        /// <summary>
        /// Any tadpoles that have been healthy enough are evolved
        /// </summary>
        public void EvolveValidTadpoles()
        {
            foreach (Tadpole tadpole in tadpoles)
            {
                tadpole.Evolve();
            }
        }

        

        /// <summary>
        /// The total number of tadpoles
        /// </summary>
        /// <returns>The number of tadpoles</returns>
        public int GetTadpoleCount()
        {
            RemoveNullItemsFromLists();
            return tadpoles.Count;
        }

        public int GetDragonflyCount()
        {
            return dragonflies.Count();
        }

        /// <summary>
        /// The number of tadpoles considered to be healthy
        /// </summary>
        /// <returns>Healthy tadpole count</returns>
        public int GetHealthyTadpoleCount()
        {
            int count = 0;

            foreach (Tadpole tadpole in tadpoles)
            {
                if (tadpole.ReadyToEvolve)
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Load the saved game
        /// </summary>
        public void LoadGame(GameObject tadpolePrefab, GameObject dragonflyPrefab, GameObject lilypadPrefab)
        {
            foreach(Tadpole tadpole in tadpoles)
            {
                GameObject.Destroy(tadpole.gameObject);
            }

            foreach (Dragonfly dragonfly in dragonflies)
            {
                GameObject.Destroy(dragonfly);
            }

            foreach (Lilypad lilypad in lilypads)
            {
                GameObject.Destroy(lilypad);
            }

            GameLoadSaver loadSaver = new GameLoadSaver();
            SaveItem saveItem = loadSaver.LoadSavedData();

            tadpoles = saveItem.Tadpoles;
            dragonflies = saveItem.Dragonflies;
            lilypads = saveItem.Lilypads;


            foreach (Tadpole tadpole in tadpoles)
            {
                GameObject tempObj = GameObject.Instantiate(tadpolePrefab);
                Tadpole existingTadpole = tempObj.AddComponent<Tadpole>();
                //existingTadpole.LoadFromTadpole(tadpole);
            }

            foreach (Dragonfly dragonfly in dragonflies)
            {
                //as with tadpole
            }

            foreach (Lilypad lilypad in lilypads)
            {
                //as with tadpole
            }
        }

        /// <summary>
        /// Save the game to the disk
        /// </summary>
        public void SaveGame()
        {
            SaveItem saveItem = new SaveItem(tadpoles,lilypads,dragonflies);
            GameLoadSaver loadSaver = new GameLoadSaver(saveItem);
            loadSaver.SaveData();
        }

        /// <summary>
        /// Makes the map of temperatures visible
        /// </summary>
        public void ShowTemperatureMap()
        {
            if (mapParent != null)
            {
                mapParent.SetActive(true);
            }
        }

        /// <summary>
        /// Hides the map of temperatures
        /// </summary>
        public void HideTemperatureMap()
        {
            if (mapParent != null)
            {
                mapParent.SetActive(false);
            }
        }

        /// <summary>
        /// Create a disturbance in the water
        /// </summary>
        /// <param name="position">The position of the disturbance</param>
        public void DisturbanceAt(Vector3 position)
        {
            float distanceThreshold = 7.0f;
            List<Tadpole> nearbyTadpoles = GetNearbyTadpoles(position, distanceThreshold);

            float distanceBetween;
            foreach(Tadpole tadpole in nearbyTadpoles)
            {
                if(tadpole.State.GetType() != typeof(GrowthStateFour))
                {
                    distanceBetween = Vector3.Distance(position, tadpole.transform.position);
                    tadpole.MoveAway(distanceBetween, position.x, position.y, position.z);
                }                
            }

            List<Lilypad> nearbyLilypad = GetNearbyLilypads(position, distanceThreshold, false);

            foreach (Lilypad lilypad in nearbyLilypad)
            {
                distanceBetween = Vector3.Distance(position, lilypad.transform.position);
                lilypad.MoveAway(distanceBetween, position.x, position.y, position.z);
            }

            List<Food> nearbyFood = GetNearbyFood(position, distanceThreshold);

            foreach (Food food in nearbyFood)
            {
                distanceBetween = Vector3.Distance(position, food.transform.position);
                food.MoveAway(distanceBetween, position.x, position.y, position.z);
            }
        }
    }
}
