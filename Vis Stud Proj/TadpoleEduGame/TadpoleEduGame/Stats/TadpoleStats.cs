﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;
using UnityEngine;

namespace TadpoleEduGame.Stats
{
    /// <summary>
    /// A class that turns a tadpole's stats into a descriptive string
    /// </summary>
    public class TadpoleStats
    {
        private Tadpole tadpole;

        /// <summary>
        /// Create a new stats object with reference to a given tadpole
        /// </summary>
        /// <param name="tadpole"></param>
        public TadpoleStats(Tadpole tadpole)
        {
            this.tadpole = tadpole;
        }

        /// <summary>
        /// Returns a string descibing in first person if the tadpole is too hot, too cold or just right
        /// </summary>
        /// <returns>A description of the tadpole's temperature</returns>
        public string GetTadpoleTemperature()
        {
            string response = "";

            float difference = tadpole.Temperature - tadpole.IdealTemperature; //if difference -ve, colder. if +ve, warmer.

            if (difference < -2.0f) //too cold
            {
                response = "Too cold!";
            }
            else if (difference > 2.0f) //too hot
            {
                response = "Too hot";
            }
            else //good temperature
            {
                response = "Just right!";
            }
            if(tadpole.ReadyToEvolve)
            {
                response = "Just right!";
            }
            return response;
        }

        /// <summary>
        /// Returns a string describing in first person the tadpole's fear level
        /// </summary>
        /// <returns>The description of how scared the tadpole is</returns>
        public string GetTadpoleFear()
        {
            string response;

            if (tadpole.FearLevel > 0)
            {
                response = "Scared of the dragonflies!";
            }
            else
            {
                response = "Not scared at all";
            }
            if (tadpole.ReadyToEvolve)
            {
                response = "Not scared at all";
            }
            return response;
        }

        /// <summary>
        /// Returns a description in first person of how hungry the tadpole is
        /// </summary>
        /// <returns>the description of the hunger level</returns>
        public string GetHungerLevel()
        {
            string response;

            if (tadpole.HungerLevel >= 1)
            {
                response = "I'm hungry, feed me!";
            }
            else if (tadpole.HungerLevel > 0.1f)
            {
                response = "I'm a little hungry";
            }
            else
            {
                response = "I'm full up";
            }
            if (tadpole.ReadyToEvolve)
            {
                response = "I'm full up";
            }
            return response;
        }

        /// <summary>
        /// The name of the tadpole associated with these stats
        /// </summary>
        /// <returns></returns>
        public string GetName()
        {
            return tadpole.Name;
        }
    }
}
