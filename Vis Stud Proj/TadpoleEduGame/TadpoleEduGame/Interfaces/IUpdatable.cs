﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TadpoleEduGame.Interfaces
{
    /// <summary>
    /// Updatabe objects should implement this
    /// </summary>
    interface IUpdatable
    {
        /// <summary>
        /// The object is updated
        /// </summary>
        void UpdateItem();
    }
}
