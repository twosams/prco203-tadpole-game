﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TadpoleEduGame.Objects;

namespace TadpoleEduGame.Interfaces
{
    /// <summary>
    /// Allows an object to be 'eaten'
    /// </summary>
    interface IEdible
    {
        /// <summary>
        /// Destroys the object. The eater is passed in as an arguament
        /// </summary>
        /// <param name="eater">The eater of this game object</param>
        void BeEaten(Entity eater);
    }
}
