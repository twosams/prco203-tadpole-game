﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TadpoleEduGame.Interfaces
{
    /// <summary>
    /// Objects that can be moved by disturbances in the water implement this
    /// </summary>
    interface IWaterMovable
    {
        /// <summary>
        /// The object moves away from a given disturbance location
        /// </summary>
        /// <param name="distanceFromWaterMove">The distance from this object to the water disturbance</param>
        /// <param name="x">Disturbance location X</param>
        /// <param name="y">Disturbance location Y</param>
        /// <param name="z">Disturbance location Z</param>
        void MoveAway(float distanceFromWaterMove, float x, float y, float z);
    }
}
