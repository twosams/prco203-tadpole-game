﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TadpoleEduGame.Interfaces
{
    /// <summary>
    /// An object that is movable would implement this
    /// </summary>
    interface IMoveable
    {
        /// <summary>
        /// The object moves towards the given target
        /// </summary>
        /// <param name="targetPosition">The target that the object should move towards</param>
        void HeadTowards(Vector3 targetPosition);
    }
}
