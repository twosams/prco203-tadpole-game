﻿using UnityEngine;
using System.Collections;
using TadpoleEduGame;
using TadpoleEduGame.Objects;
using TadpoleEduGame.Stats;
using UnityEngine.UI;

public class TadpoleStats : MonoBehaviour 
{
    //Reference to the Tadpole
    private Tadpole tadpoleClicked;
    private TadpoleEduGame.Stats.TadpoleStats tadpoleStats;

    private bool firstClick;
    public Text foodText;
    public Text scaredText;
    public Text tempText;
    public Text nameText;
    public GameObject unclickedParent;
    public GameObject clickedParent;
    public GameObject tadpoleHighlightObject;

    private GameObject highlightObj;

	// Use this for initialization
	void Start () 
    {
        firstClick = true;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit;

            hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.down, 30);
            if (hit.transform != null && hit.transform.CompareTag("Tadpole"))
            {
                tadpoleClicked = hit.transform.GetComponent<Tadpole>();
                tadpoleStats = new TadpoleEduGame.Stats.TadpoleStats(tadpoleClicked);

                if (firstClick == true)
                {
                    unclickedParent.SetActive(false);
                    clickedParent.SetActive(true);
                    firstClick = false;
                }
                UpdateText();
                //Debug.LogWarning("UI - " + tadpoleStats.GetName());

                if (highlightObj != null)
                {
                    GameObject.Destroy(highlightObj);
                }

                highlightObj = GameObject.Instantiate(tadpoleHighlightObject);
                highlightObj.transform.parent = tadpoleClicked.transform;
                highlightObj.transform.localPosition = Vector3.zero;
                highlightObj.transform.localRotation = Quaternion.Euler(Vector3.zero);
            }
            else //clicked on a non-tadpole
            {
                if (firstClick != true)
                {
                    unclickedParent.SetActive(true);
                    clickedParent.SetActive(false);
                    firstClick = true;

                    if (highlightObj != null)
                    {
                        GameObject.Destroy(highlightObj);
                    }
                    tadpoleClicked = null;
                }
            }
            if(tadpoleClicked != null)
            {
                UpdateText();
            }
        }
	}

    private void UpdateText()
    {
        foodText.text = tadpoleStats.GetHungerLevel();
        scaredText.text = tadpoleStats.GetTadpoleFear();
        tempText.text = tadpoleStats.GetTadpoleTemperature();
        nameText.text = tadpoleClicked.Name;
    }
}
