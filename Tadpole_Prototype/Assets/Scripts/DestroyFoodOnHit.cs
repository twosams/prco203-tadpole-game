﻿using UnityEngine;
using System.Collections;
using TadpoleEduGame.Objects;

public class DestroyFoodOnHit : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Food"))
        {
            GameObject.Destroy(col.gameObject);
        }

    }
}
