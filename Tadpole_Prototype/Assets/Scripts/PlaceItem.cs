﻿using UnityEngine;
using TadpoleEduGame.Objects;
using TadpoleEduGame;
using System.Collections;

public class PlaceItem : MonoBehaviour {

    public GameObject Food;
    public GameObject Lilypad;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
        //Currently unable to eat as they are never hungry
        if(Input.GetKeyDown(KeyCode.F))
        {
            GameObject createdFood = CreateItemAt(Food, Input.mousePosition, -10);
            Food foodRef = createdFood.GetComponent<Food>();

            if (foodRef == null)
            {
                foodRef = createdFood.AddComponent<Food>();
            }

            PondEnvironment.Instance.AddFoodReference(foodRef);
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            GameObject createdLilypad = CreateItemAt(Lilypad, Input.mousePosition, -10);
            Lilypad lilypadRef = createdLilypad.GetComponent<Lilypad>();

            if (lilypadRef == null)
            {
                lilypadRef = createdLilypad.AddComponent<Lilypad>();
            }

            PondEnvironment.Instance.AddLilypadReference(lilypadRef);
        }
	}

    private GameObject CreateItemAt(GameObject item, Vector3 position, float depth)
    {
        GameObject createdItem = GameObject.Instantiate(item);
        createdItem.transform.position = Camera.main.ScreenToWorldPoint(position);
        Vector3 newPos = new Vector3(createdItem.transform.position.x, createdItem.transform.position.y, depth);
        createdItem.transform.position = newPos;

        return createdItem;
    }
}
