﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour {
        
    GameObject clockFace;
    GameObject sun;    
    [Range(0, 30)]
    public float modifier = 10;
    

    void Start()
    {        
        clockFace = GameObject.Find("Clock_Inner");
        sun = GameObject.Find("Sun");      
        //starting rotation for clockFace [0,0,0]
        //starting rotation for sun [0,90,0]
    }

    public void UpdateItem()
    {
        clockFace.transform.Rotate(Vector3.forward * modifier * Time.deltaTime);
        sun.transform.Rotate(Vector3.down * modifier * Time.deltaTime);
    }  
}
