﻿using UnityEngine;
using System.Collections;

public class LilypadTrigger : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Something enter");
        if (other.gameObject.tag == "Tadpole")
        {
            Debug.Log("Tadpole enter");
            ChangeTransparency(0.3f);
        }
    }

    public void ChangeTransparency(float transparency)
    {
        SpriteRenderer spritRend = this.transform.parent.GetComponent<SpriteRenderer>();
        spritRend.color = new Color(spritRend.color.r, spritRend.color.g, spritRend.color.b, transparency);
    }
}
