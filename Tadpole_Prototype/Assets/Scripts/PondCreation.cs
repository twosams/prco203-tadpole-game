﻿using UnityEngine;
using System.Collections;
using TadpoleEduGame;
using TadpoleEduGame.Objects;
using TadpoleEduGame.Utility;
using System.Collections.Generic;

public class PondCreation : MonoBehaviour {

    public GameObject waterSurface;

    public List<GameObject> tadpolesGO;

    public GameObject dragonflyBase;
    public int numDragonfly;

    public GameObject lilypadBase;
    public int numLilypads;

    public GameObject temperatureMapObject;
    public GameObject temperatureMapSpawnPoint;

    private WaterSurface waterSurfaceObject;
    private List<Tadpole> tadpoleObjects;
    private List<Dragonfly> dragonflyObjects;
    private List<Lilypad> lilypadObjects;

    public bool isPlaying = false;
    private bool objectsLoaded = false;
    private TimeManager tManager;
   
    private bool gameStarted = false;

    private float timer = 0f;

	// Use this for initialization
	void Start ()
    {
        waterSurfaceObject = waterSurface.AddComponent<WaterSurface>();
        tManager = GameObject.Find("Managers").GetComponent<TimeManager>();
        loadLilypads();
    }
	
	// Update is called once per frame
	void Update ()
    {        
        if (isPlaying)
        {
            PondEnvironment.Instance.UpdateItem();
            tManager.UpdateItem();
        }
    }

    public void startGame()
    {
        if(!gameStarted)
        {
            loadObjects();
        }
        resumeGame();
    }

    public void pauseGame()
    {
        isPlaying = false;
    }

    public void resumeGame()
    {
        isPlaying = true;        
    }

    public void toggleGame()
    {
        if(isPlaying)
        {
            pauseGame();
        }
        else
        {
            resumeGame();
        }
    }

    private void loadTadpoles()
    {
        //Debug.Log("loading tadpoles");
        List<Tadpole> tadpoleTadpoles = new List<Tadpole>();

        foreach (GameObject tadpole in (GameObject[])GameObject.FindGameObjectsWithTag("Tadpole"))
        {
            Tadpole newTadpole = tadpole.AddComponent<Tadpole>();
            tadpolesGO.Add(tadpole);
            tadpoleTadpoles.Add(newTadpole);
        }

        PondEnvironment.Instance.LoadTadpoleList(tadpoleTadpoles);
    }

    private void loadLilypads()
    {

        GameObject tempObj;
        lilypadObjects = new List<Lilypad>();

        for (int count = 0; count < numLilypads; count++)
        {
            Vector3 randomPoint = PositionUtility.NewRandomTarget();
            randomPoint = PositionUtility.LimitPositionTo(32.0f, 21.0f, -5.0f, -31.0f, -17.0f, -Mathf.Infinity, randomPoint);//limit to pond area
            randomPoint.z = -10; //then fix to surface
            tempObj = (GameObject)Instantiate(lilypadBase, randomPoint, new Quaternion(0, 0, 0, 0));
            lilypadObjects.Add(tempObj.AddComponent<Lilypad>());
        }
        PondEnvironment.Instance.LoadLilypadList(lilypadObjects);
    }

    private void loadDragonflies()
    {

        GameObject tempObj;
        dragonflyObjects = new List<Dragonfly>();

        for (int count = 0; count < numDragonfly; count++)
        {
            Vector3 randomPoint = PositionUtility.NewRandomTarget();
            randomPoint.z = -14; //hovering above the water
            tempObj = (GameObject)Instantiate(dragonflyBase, randomPoint, new Quaternion(0, 0, 0, 0));
            dragonflyObjects.Add(tempObj.AddComponent<Dragonfly>());

        }

        PondEnvironment.Instance.LoadDragonflyList(dragonflyObjects);
    }
    
    private void loadObjects()
    {
        objectsLoaded = true;
        loadTadpoles();
        loadDragonflies();
        gameStarted = true;
        PondEnvironment.Instance.LoadWaterSurface(waterSurfaceObject);
        //PondEnvironment.Instance.LoadItemLists(tadpoleObjects, dragonflyObjects, lilypadObjects, waterSurfaceObject);
        PondEnvironment.Instance.GenerateTemperatures(temperatureMapObject, temperatureMapSpawnPoint.transform.position);
    }

    public void pubLoadDragonflies()
    {
        loadDragonflies();
    }

    /*
    
    //{
    //    GameObject tempObj;// = (GameObject)Instantiate(waterSurface, new Vector3(0,20, 0), new Quaternion(0, 0, 0, 0));
    //    waterSurfaceObject = waterSurface.AddComponent<WaterSurface>();
    //    tManager = GameObject.Find("Managers").GetComponent<TimeManager>();

    //    tadpoleObjects = new List<Tadpole>();
    //    dragonflyObjects = new List<Dragonfly>();
        

    //    for (int count = 0; count < numTadpoles; count++)
    //    {
    //        Vector3 position = PositionUtility.NewRandomTarget();
    //        position = PositionUtility.LimitPositionTo(32.0f, 21.0f, -5.0f, -31.0f, -17.0f, -Mathf.Infinity, position); //fix to pond area
    //        tempObj = (GameObject)Instantiate(tadpoleBase, position, new Quaternion(0, 0, 0, 0));
    //        tadpoleObjects.Add(tempObj.AddComponent<Tadpole>());
    //    }

    //    for (int count = 0; count < numDragonfly; count++)
    //    {
    //        Vector3 randomPoint = PositionUtility.NewRandomTarget();
    //        randomPoint.z = -11; //hovering above the water
    //        tempObj = (GameObject)Instantiate(dragonflyBase, randomPoint, new Quaternion(0, 0, 0, 0));
    //        dragonflyObjects.Add(tempObj.AddComponent<Dragonfly>());
    //    }

        
    */

}
