﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TadpoleEduGame.Objects;
using TadpoleEduGame;

public class CursorItem : MonoBehaviour
{
    private CanvasGroup cGroup;
    private bool enabled;
    public GameObject[] goItems;
    private float depth = -10;
    private System.Random randomVal = new System.Random();
    public InterfaceManager iManager;
    private int lilypadNumber;
    
    void Start()
    {
        enabled = false;
        cGroup = this.GetComponent<CanvasGroup>();
    }

    void Update()
    {
        if(enabled)
        {
            transform.position = Input.mousePosition;
            if(Input.GetMouseButtonDown(0))
            {
                if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(-1))
                {
                    
                    foreach(GameObject item in goItems)
                    {
                        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);                        
                        GameObject newItem = (GameObject)GameObject.Instantiate(item, new Vector3(mousePos.x + (float)randomVal.NextDouble(), mousePos.y + (float)randomVal.NextDouble(), depth), Quaternion.identity);
                        newItem.name = newItem.name + lilypadNumber;
                        lilypadNumber++;

                        AssignType(newItem);
                    }
                }
                Toggle();
                iManager.dropItem();
            }
        }
    }

    public void Toggle()
    {
        if(enabled)
        {
            enabled = false;
            cGroup.alpha = 0;
        }
        else if (!enabled)
        {
            enabled = true;
            cGroup.alpha = 0.4f;
        }
    }
	
    private void AssignType(GameObject obj)
    {
        if (obj.CompareTag("Food"))
        {
            Food foodRef = obj.AddComponent<Food>();
            PondEnvironment.Instance.AddFoodReference(foodRef);
        }
        if (obj.CompareTag("Lilypad"))
        {

            Lilypad lilyRef = obj.AddComponent<Lilypad>();
            PondEnvironment.Instance.AddLilypadReference(lilyRef);
        }
    }
}
