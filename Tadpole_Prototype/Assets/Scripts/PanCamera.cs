﻿using UnityEngine;
using System.Collections;

public class PanCamera : MonoBehaviour
{    
    [Range(0.5f, 2.0f)]
    public float panSpeed = 1;
    public float minX = -24.5f, minY = -10.5f, maxX = 24.5f, maxY = 10.5f;

	void Start ()
    {
	
	}
	
	void Update ()
    {
	    if(Input.GetMouseButton(1))
        {
            float h = panSpeed * Input.GetAxis("Mouse X");
            float v = panSpeed * Input.GetAxis("Mouse Y");
            transform.Translate(-h, -v, 0);

            Clamp();
        }
	}

    private void Clamp()
    {
        if(transform.position.x < minX)
        {
            transform.position = new Vector3(minX, transform.position.y, transform.position.z);
        }
        if (transform.position.x > maxX)
        {
            transform.position = new Vector3(maxX, transform.position.y, transform.position.z);
        }
        if (transform.position.y < minY)
        {
            transform.position = new Vector3(transform.position.x, minY, transform.position.z);
        }
        if (transform.position.y > maxY)
        {
            transform.position = new Vector3(transform.position.x, maxY, transform.position.z);
        }
    }
}
