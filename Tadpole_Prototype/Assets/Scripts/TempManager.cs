﻿using UnityEngine;
using System.Collections;
using TadpoleEduGame;
using TadpoleEduGame.Objects;

public class TempManager : MonoBehaviour
{
    private bool isMapVisible = false;
    private GameObject btnShowMap;
    private GameObject btnHideMap;

    void Start()
    {
       
    }

    void Update()
    {
        
    }

    public void TemperatureMapOff()
    {
        isMapVisible = false;
        PondEnvironment.Instance.HideTemperatureMap();
    }

    public void TemperatureMapOn()
    {
        isMapVisible = true;
        PondEnvironment.Instance.ShowTemperatureMap();
    }	
}
