﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ThermoCursor : MonoBehaviour {

    private GameObject tCursor;
    private Image tCursorFill;
    private Text cursorText;
    private Vector3 offset = new Vector3(16,32,0);
    private TemperatureAtMouse temp;

	// Use this for initialization
	void Start () {
        temp = GameObject.Find("Managers").GetComponent<TemperatureAtMouse>();
        cursorText = transform.GetChild(1).gameObject.GetComponent<Text>();
        tCursorFill = transform.GetChild(0).gameObject.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = Input.mousePosition + offset;
        tCursorFill.fillAmount = temp.GetMouseTemperature() / 30.0F;
        cursorText.text = temp.GetMouseTemperature().ToString() + "°C";
    }


}
