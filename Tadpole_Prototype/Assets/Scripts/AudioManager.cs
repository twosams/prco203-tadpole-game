﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour {

    public AudioMixer master;

    public void SetFxVol(float fxVol)
    {
        master.SetFloat("fxVol", fxVol);
    }

    public void SetMusVol(float musVol)
    {
        master.SetFloat("musVol", musVol);
    }
}
