﻿using UnityEngine;
using System.Collections;

public class TimeControl : MonoBehaviour 
{
    //Declare Variables
    public float speed = 10;
	
	// Update is called once per frame
	void Update () 
    {
        RotateSun();
	}

    private void RotateSun()
    {
        transform.Rotate(Vector3.up * (speed * Time.deltaTime));
    }

    public float GetSunRotation()
    {
        return transform.rotation.y;
    }
}
