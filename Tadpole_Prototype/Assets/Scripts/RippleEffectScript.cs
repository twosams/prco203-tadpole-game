﻿using UnityEngine;
using System.Collections;

public class RippleEffectScript : MonoBehaviour
{
    private Vector3 mScale;
    private Vector3 endScale;
    private float lerpRate = 1f;
    private bool doLerp = false;
    private float lerpVal = 0.0f;
    private SpriteRenderer sprRenderer;

    void Start()
    {
        sprRenderer = this.GetComponent<SpriteRenderer>();
        
        Initialise();
    }

    void Update()
    {
        //If loop grows the ripple to a lerp value of 1, then destroys the ripple
        if (doLerp)
        {
            if (lerpVal < 1)
            {
                lerpVal += Time.deltaTime * lerpRate;
                transform.localScale = Vector3.Lerp(mScale, endScale, lerpVal);
                sprRenderer.color = new Color(sprRenderer.color.r,sprRenderer.color.b,sprRenderer.color.g, 1- lerpVal);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    void Initialise()
    {
        mScale = this.transform.localScale;
        endScale = mScale * 30;
        
        lerpVal = 0.0f;
        doLerp = true;
        
    }

    


    

    

    
	
}
