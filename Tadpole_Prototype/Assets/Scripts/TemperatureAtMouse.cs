﻿using UnityEngine;
using System.Collections;
using TadpoleEduGame;

public class TemperatureAtMouse : MonoBehaviour {

    public bool on = false;

    private float temperature = 0.0f;
    private GameObject cursor;
    private CanvasGroup cursorGroup;

	// Use this for initialization
	void Start () {
        cursor = GameObject.Find("tempCurs");
        cursorGroup = cursor.GetComponent<CanvasGroup>();
        cursorGroup.alpha = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (on)
        {
            temperature = PondEnvironment.Instance.GetTemperatureAt(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
	}

    /// <summary>
    /// Get the temperature the mouse is currently at
    /// </summary>
    /// <returns></returns>
    public float GetMouseTemperature()
    {
        return temperature;
    }

    public void ToggleState()
    {
        if(on)
        {
            cursorGroup.alpha = 0;
            on = false;
        }
        else
        {
            cursorGroup.alpha = 1;
            on = true;
        }
    }
}
