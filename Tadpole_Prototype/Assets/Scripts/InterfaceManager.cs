﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class InterfaceManager : MonoBehaviour
{

    public GameObject cgMenu;
    public GameObject cgCursor;
    public GameObject cgButtons;
    public GameObject cgStats;
    public GameObject cgSettings;
    public GameObject cgFail;
    public GameObject cgWin;
    public GameObject cgComplete;

    public CursorItem itemHeld;

    void Start()
    {
        //Tutorial here, method best

        //ToggleCanvasGroup(cgCursor);
        ToggleCanvasGroup(cgButtons);
        //ToggleCanvasGroup(cgStats);
        ToggleCanvasGroup(cgSettings);
        ToggleCanvasGroup(cgFail);
        ToggleCanvasGroup(cgWin);
        //ToggleCanvasGroup(cgComplete);        
    }

    public void ToggleCanvasGroup(GameObject group)
    {
        CanvasGroup canvasGroup = group.GetComponent<CanvasGroup>();
        if (canvasGroup.interactable == false)
        {
            canvasGroup.alpha = 1;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
        }
        else if (canvasGroup.interactable == true)
        {
            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }
    }    

    public void ToggleOnlyInteractable(GameObject group)
    {
        CanvasGroup canvasGroup = group.GetComponent<CanvasGroup>();
        if (canvasGroup.interactable == false)
        {
            canvasGroup.interactable = true;
        }
        else if (canvasGroup.interactable == true)
        {
            canvasGroup.interactable = false;
        }
    }

    public CursorItem getItem()
    {
        return itemHeld;
    }

    public void setItem(CursorItem item)
    {
        item.Toggle();
        itemHeld = item;
    }
    
    public void dropItem()
    {       
        itemHeld = null;
    }


}
