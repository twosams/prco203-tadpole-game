﻿using UnityEngine;
using TadpoleEduGame.Objects;
using TadpoleEduGame;
using System.Collections.Generic;
using System.Collections;

public class AddFood : MonoBehaviour {

    private Vector3 foodTransform;
    private Transform[] spawnPoints;
    public List<GameObject> foodPieces;
    public System.Random randomValue = new System.Random();
    private GameObject cursor;
    private CanvasGroup cursorGroup;
    public bool on = false;

    private float depth = -10;

    
    private Rect rect = new Rect(0, 0, 100, 50);

	// Use this for initialization
	void Start () {
        cursor = GameObject.Find("foodCurs");
        cursorGroup = cursor.GetComponent<CanvasGroup>();
        cursorGroup.alpha = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if(on)
        {
            
            if (Input.GetMouseButtonDown(0))
            {
                foreach (GameObject foodPiece in foodPieces)
                {
                    GameObject createdItem = GameObject.Instantiate(foodPiece);

                    Vector3 mousePos = Input.mousePosition;
                    createdItem.transform.position = Camera.main.ScreenToWorldPoint(mousePos);
                    Vector3 newPos = new Vector3(createdItem.transform.position.x + (float)randomValue.NextDouble(), createdItem.transform.position.y + (float)randomValue.NextDouble(), depth);
                    createdItem.transform.position = newPos;
                    Food foodRef = createdItem.AddComponent<Food>();

                    PondEnvironment.Instance.AddFoodReference(foodRef);
                }
                on = false;
                cursorGroup.alpha = 0;
            }
        }
	}


    public void ToggleState()
    {
        if (on)
        {
            cursorGroup.alpha = 0;
            on = false;
            
        }
        else
        {
            cursorGroup.alpha = 0.4f;
            on = true;

        }
    }



}
