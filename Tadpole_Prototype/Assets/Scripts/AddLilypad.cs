﻿using UnityEngine;
using System.Collections;
using TadpoleEduGame.Objects;
using TadpoleEduGame;
using System.Collections.Generic;

public class AddLilypad : MonoBehaviour {

    private Vector3 foodTransform;
    private Transform[] spawnPoints;
    public List<GameObject> lilyPieces;
    public System.Random randomValue = new System.Random();
    private GameObject cursor;
    private CanvasGroup cursorGroup;
    public bool on = false;

    private float depth = -10;


    private Rect rect = new Rect(0, 0, 100, 50);

    // Use this for initialization
    void Start()
    {
        cursor = GameObject.Find("padCurs");
        cursorGroup = cursor.GetComponent<CanvasGroup>();
        cursorGroup.alpha = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (on)
        {
            if (Input.GetMouseButtonDown(0))
            {
                int lilypadNumber = 1;

                foreach (GameObject lilyPiece in lilyPieces)
                {

                    GameObject createdItem = GameObject.Instantiate(lilyPiece);

                    Vector3 mousePos = Input.mousePosition;
                    createdItem.transform.position = Camera.main.ScreenToWorldPoint(mousePos);
                    createdItem.name = createdItem.name + lilypadNumber;
                    lilypadNumber++;
                    Vector3 newPos = new Vector3(createdItem.transform.position.x + (float)randomValue.NextDouble(), createdItem.transform.position.y + (float)randomValue.NextDouble(), depth);
                    createdItem.transform.position = newPos;
                    Lilypad lilyRef = createdItem.AddComponent<Lilypad>();

                    PondEnvironment.Instance.AddLilypadReference(lilyRef);
                }
                on = false;
                cursorGroup.alpha = 0;
            }
        }
    }


    public void ToggleState()
    {
        if (on)
        {
            cursorGroup.alpha = 0;
            on = false;

        }
        else
        {
            cursorGroup.alpha = 0.4f;
            on = true;

        }
    }
}
