﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InstructionsManager : MonoBehaviour 
{
    //References to the Instruction Screen
    public Text subTitle;
    public Text instructionText;
    public Image instructionSprite;
    public Image menuSprite;
    public Sprite TadpoleSprite;
    public Sprite ClockSprite;
    public Sprite LilypadSprite;
    public Sprite FoodSprite;
    public Sprite TemperatureSprite;


    //Array to hold the different instruction texts
    private string[] instructionArray = new string[5];
    private string[] subtitleArray = new string[5];
    private Sprite[] spriteArray = new Sprite[5];

    //Current Screen to Show
    private int currentInstruction = 0;




	// Use this for initialization
	void Start () 
    {
        InitArray();
        NextScreen();
	}

    public void NextScreen()
    {
        if (currentInstruction < subtitleArray.Length)
        {
            subTitle.text = subtitleArray[currentInstruction];
            instructionText.text = instructionArray[currentInstruction];
            instructionSprite.sprite = spriteArray[currentInstruction];

            currentInstruction++;
        }
    }

    private void InitArray()
    {

        //Set the Subtitles

        for (int i = 0; i <= subtitleArray.Length; i++)
        {
            if(i == 0)
            {
                subtitleArray[i] = "General";
            }

            if (i == 1)
            {
                subtitleArray[i] = "Day and Night";
            }

            if (i == 2)
            {
                subtitleArray[i] = "Lilypads";
            }

            if (i == 3)
            {
                subtitleArray[i] = "Food";
            }

            if (i == 4)
            {
                subtitleArray[i] = "Temperature";
            }
        }

        //Set the Instruction Screens

        for (int i = 0; i <= instructionArray.Length; i++)
        {
            if (i == 0)
            {
                instructionArray[i] = "The Goal is to keep your tadpoles healthy and alive long enough for them to become frogs! @@ To do this you must keep them safe from the Dragonflys that want to eat them, Feed them regularly and make sure they stay at the right temperature @@ If you keep them healthy long enough, they'll progress to the next level of development, well on the way to growing up big and strong!";
                instructionArray[i] = instructionArray[i].Replace("@", System.Environment.NewLine);
            }

            if (i == 1)
            {
                instructionArray[i] = "This clock shows whether it is day or night. @@ Every morning, it may be hotter or colder then the day before, so remember to check regularly as the ponds temperature WILL change.";
                instructionArray[i] = instructionArray[i].Replace("@", System.Environment.NewLine);
            }

            if (i == 2)
            {
                instructionArray[i] = "Lilypads are important for a tadpoles development, they can hide under them to protect them from predators such as dragonflies and they use them for shade if it is too warm. @@ If a dragonfly lands on a lilypad, click them to tell them to go away!";
                instructionArray[i] = instructionArray[i].Replace("@", System.Environment.NewLine);
            }

            if (i == 3)
            {
                instructionArray[i] = "A growing tadpole needs to eat regularly @@ Click the Button shown here and drag the food onto the pond surface to feed them! A hungry tadpole might not live long enough to become a frog";
                instructionArray[i] = instructionArray[i].Replace("@", System.Environment.NewLine);
            }

            if (i == 4)
            {
                instructionArray[i] = "Temperature. @@ You can check the Temperature of the water by clicking on the button shown and dragging the mouse around the pond @@ You need to herd the tadpoles into water that is the perfect temperature for them, you can do this by clicking the water and scaring them.";
                instructionArray[i] = instructionArray[i].Replace("@", System.Environment.NewLine);
            }
        }

        //Set the Sprite List

        for (int i = 0; i <= instructionArray.Length; i++)
        {
            if (i == 0)
            {
                spriteArray[i] = TadpoleSprite;
            }

            if (i == 1)
            {
                spriteArray[i] = ClockSprite;
            }

            if (i == 2)
            {
                spriteArray[i] = LilypadSprite;
            }

            if (i == 3)
            {
                spriteArray[i] = FoodSprite;
            }

            if (i == 4)
            {
                spriteArray[i] = TemperatureSprite;
            }
        }
    }
}
