﻿using UnityEngine;
using System.Collections;

public class PopUpManager : MonoBehaviour 
{

    public GameObject window;

    float scale = 0.5f;
    float maxScale = 10.0f;
    float scaleSpeed = 1f;

	// Use this for initialization
	void Start () 
    {

	}

    void Update()
    {
        //Grow the Window
        scale += (scaleSpeed * Time.deltaTime);

        if(scale > maxScale)
        {
            scale = maxScale;
        }

        Vector3 scaleVector = new Vector3(scale, scale, scale);

        window.transform.localScale = scaleVector;

    }
}
