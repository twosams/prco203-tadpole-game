﻿using UnityEngine;
using System.Collections;
using TadpoleEduGame;
using TadpoleEduGame.Objects;

public class PondTester : MonoBehaviour {

    int counter = 0;
    bool saved = false;
    bool loaded = false;

    public GameObject mapObject;
    public GameObject tadpole;
    public GameObject lilypad;
    public GameObject dragonfly;

	// Use this for initialization
	void Start () {
        //PondEnvironment.Instance.GenerateTemperatures(mapObject, FindObjectOfType<WaterSurface>().transform.position);
	}
	
	// Update is called once per frame
	void Update () {

        if (counter % 50 == 0)
        {
            Debug.Log(counter);
        }

        counter++;

        if (counter >= 400 && !saved)
        {
            PondEnvironment.Instance.SaveGame();
        }

        if(saved && counter >= 450 && !loaded)
        {
            loaded = true;
            PondEnvironment.Instance.LoadGame(tadpole, dragonfly, lilypad);
        }
	}

    public void TemperatureMapOff()
    {
        PondEnvironment.Instance.HideTemperatureMap();
    }

    public void TemperatureMapOn()
    {
        PondEnvironment.Instance.ShowTemperatureMap();
    }
}
