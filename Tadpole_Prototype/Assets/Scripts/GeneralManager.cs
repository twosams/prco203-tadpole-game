﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TadpoleEduGame;
using UnityEngine.UI;
using TadpoleEduGame.Objects;

public class GeneralManager : MonoBehaviour 
{
    //-----------------
    //Declare Variables
    //-----------------
    
    public GameObject stageOneComplete;
    public GameObject stageTwoComplete;
    public GameObject stageThreeComplete;
    public GameObject stageFourComplete;
    public GameObject pondObj;
    public GameObject gameWon;
    public GameObject gameLost;
    public Text healthyTadpoles;

    bool gameWonAlready = false;
    bool gameLostAlready = false;

    private PondCreation pondCreation;
    private InterfaceManager iMan;
    //Pond Behaviour Reference
    private PondEnvironment pondEnvironment;

    //Number of Tadpoles
    private int tadpoleNum;

    //Number of healthy Tadpoles
    private int healthyTadpoleNum;

    //Current Evolution Level
    private int evoLevel;

    //Max Level
    private int maxEvoLevel;

    //Points Required to Evolve
    private int evoPointsRequired;

    //Current Points
    [Range(0,4)]
    public float currentEvoPoints;

	// Use this for initialization
	void Start ()
    {
	    //Initialize the Variables
        iMan = GetComponent<InterfaceManager>();
        pondEnvironment = PondEnvironment.Instance;
        pondCreation = pondObj.GetComponent<PondCreation>();
        tadpoleNum = pondEnvironment.GetTadpoleCount();
        iMan.ToggleCanvasGroup(stageOneComplete);
        iMan.ToggleCanvasGroup(stageTwoComplete);
        iMan.ToggleCanvasGroup(stageThreeComplete);
        healthyTadpoleNum = pondEnvironment.GetTadpoleCount();

        evoLevel = 1;

        maxEvoLevel = 4;
        evoPointsRequired = 4;
        currentEvoPoints = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {        
        
        //Run AddEvolutionPoints 30 times per Second
        AddEvolutionPoints();

        healthyTadpoles.text = "Healthy Tadpoles = " + pondEnvironment.GetHealthyTadpoleCount();

        //If the Current Evolution Points are higher then the Required Points 
        if(currentEvoPoints >= evoPointsRequired)
        {
            //Evolve all Healthy Tadpoles
            pondEnvironment.EvolveValidTadpoles();

            //Delete all UnHealthy Tadpoles
            pondEnvironment.RemoveTadpolesAtStage(evoLevel);            
            
            if (evoLevel < maxEvoLevel)
            {
                //Debug.LogWarning(PondEnvironment.Instance.GetTadpoleCount());
                
                if(pondEnvironment.GetTadpoleCount() != 0)
                {
                    if(evoLevel == 1)
                    {
                        cleanDragonfly();

                        iMan.ToggleCanvasGroup(stageOneComplete);
                        pondCreation.toggleGame();
                        evoLevel++;
                        currentEvoPoints = 0;
                    }
                    else if(evoLevel == 2)
                    {
                        cleanDragonfly();
                        
                        iMan.ToggleCanvasGroup(stageTwoComplete);
                        pondCreation.toggleGame();
                        evoLevel++;
                        currentEvoPoints = 0;
                    }
                    else if(evoLevel == 3)
                    {
                        cleanDragonfly();
                        
                        iMan.ToggleCanvasGroup(stageThreeComplete);
                        pondCreation.toggleGame();
                        evoLevel++;
                    }                                     
                }
                else
                {
                    GameLost();
                    pondCreation.toggleGame();
                }   
            }            
        }

        if(evoLevel == 4 && PondEnvironment.Instance.GetDragonflyCount() <= 0)
        {
            GameWon();
        }
	}


    private void cleanDragonfly()
    {
        foreach (Dragonfly dragonfly in PondEnvironment.Instance.GetNearbyDragonflies(Vector3.zero, Mathf.Infinity))
        {
            PondEnvironment.Instance.RemoveDragonFly(dragonfly);
            GameObject.Destroy(dragonfly.gameObject);
        }

        pondCreation.pubLoadDragonflies();

    }

    private void AddEvolutionPoints()
    {
        if(pondCreation.isPlaying)
        {
            float toBeAdded = 0.1f * Time.deltaTime;

            currentEvoPoints += toBeAdded;
        }
    }  
 
    private void GameWon()
    {
        if (!gameWonAlready)
        {
            gameWonAlready = true;
            iMan.ToggleCanvasGroup(gameWon);
        }
    }

    private void GameLost()
    {
        if(!gameLostAlready)
        {
            gameLostAlready = true;
            iMan.ToggleCanvasGroup(gameLost);  
        }              
    }    
}
