﻿using UnityEngine;
using System.Collections;
using TadpoleEduGame.Objects;
using TadpoleEduGame;

public class MoveObjectsOnClick : MonoBehaviour
{
    float radius = 5f;
    Vector3 mousePos;
    Vector2 dir;
    //float rFar = 5f, rMid = 2.5f, rNear = 1f;
    //float forceMod = 15f;
    public InterfaceManager iManager;

    void Update()
    {
        
        //Debug.DrawRay(mousePos, Vector2.right * radius);


        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = -20;
            //RaycastHit2D hit2D = new RaycastHit2D();
            //Vector3 raySource = mousePos;
            //raySource.z = -15;
            //Physics.Raycast(mousePos, Vector3.forward, out hit, Mathf.Infinity);

            if (Physics.Raycast(mousePos, Vector3.forward, out hit, Mathf.Infinity))
            {
                if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(-1))
                {
                    if (iManager.getItem() == null)                    
                    {
                        if (hit.collider.name == "WaterSurfaceCol") //only do it if it's the water that was clicked
                        {
                            StartCoroutine(SpawnRipple(hit.point));
                            PondEnvironment.Instance.DisturbanceAt(hit.point);
                        }                        
                        else if (hit.collider.tag == "bdrokenLilypad")
                        {
                            float force = 100f;
                            hit.collider.gameObject.transform.GetChild(0).GetComponent<Rigidbody2D>().AddForce(new Vector2(-1,1) * force);                            
                            hit.collider.gameObject.transform.GetChild(1).GetComponent<Rigidbody2D>().AddForce(new Vector2(-1, -1) * force);
                            hit.collider.gameObject.transform.GetChild(2).GetComponent<Rigidbody2D>().AddForce(new Vector2(1,-1) * force);
                            hit.collider.gameObject.transform.GetChild(3).GetComponent<Rigidbody2D>().AddForce(new Vector2(1, 1) * force); 
                        }
                    }
                    
                }
                
            }
            //hit2D = Physics2D.Raycast(raySource, Vector2.down, 100.0f);

            
            
            //Debug.LogWarning("3D; " + hit.collider.name);

            

            //{
            //    mousePos.z = hit.point.z;
            //    StartCoroutine(SpawnRipple(mousePos));
            //    Collider2D[] farHitColldiers = Physics2D.OverlapCircleAll(mousePos, rFar);
            //    Collider2D[] midHitColliders = Physics2D.OverlapCircleAll(mousePos, rMid);
            //    Collider2D[] nearHitColliders = Physics2D.OverlapCircleAll(mousePos, rNear);
            //    //print(farHitColldiers.Length);
            //    //print(midHitColliders.Length);
            //    //print(nearHitColliders.Length);

            //    foreach (Collider2D col in nearHitColliders)
            //    {
            //        Rigidbody2D _rb = col.GetComponent<Rigidbody2D>();
            //        {
            //            if (_rb != null)
            //            {
            //                dir = _rb.gameObject.transform.position - mousePos;
            //                StartCoroutine(AddForceToRB(0f, _rb, dir, 7.5f));
            //            }
            //        }
            //    }
            //    foreach (Collider2D col in midHitColliders)
            //    {
            //        Rigidbody2D _rb = col.GetComponent<Rigidbody2D>();
            //        {
            //            if (_rb != null)
            //            {
            //                dir = _rb.gameObject.transform.position - mousePos;
            //                StartCoroutine(AddForceToRB(0.2f, _rb, dir, 5.0f));
            //            }
            //        }
            //    }
            //    foreach (Collider2D col in farHitColldiers)
            //    {
            //        Rigidbody2D _rb = col.GetComponent<Rigidbody2D>();
            //        {
            //            if (_rb != null)
            //            {
            //                dir = _rb.gameObject.transform.position - mousePos;
            //                StartCoroutine(AddForceToRB(0.4f, _rb, dir, 2.5f));
            //            }
            //        }
            //    }
            //}
        }
    }
    private IEnumerator AddForceToRB(float delay, Rigidbody2D obj, Vector2 dir, float force)
    {
        yield return new WaitForSeconds(delay);
        obj.AddForce(dir * force);
    }

    private IEnumerator SpawnRipple(Vector3 pos)
    {
        int noOfRipples = 1;   
        
        for (int i = 0; i < noOfRipples; i++)
        {            
            GameObject go_ripple = Instantiate((GameObject)Resources.Load("ripple", typeof(GameObject))) as GameObject;
            go_ripple.transform.position = pos;
            if(i == 0)
            {
                AudioSource ripple = go_ripple.AddComponent<AudioSource>();
                ripple.clip = Resources.Load("Sounds/drops")as AudioClip;
                ripple.Play();
            }
            yield return new WaitForSeconds(0.3f);
        }
        
    }

    
}
