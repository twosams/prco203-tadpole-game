﻿using UnityEngine;
using System.Collections;

public class MenuPanel : MonoBehaviour
{    	

    public void TogglePanel()
    {
        if(this.gameObject.activeInHierarchy)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(true);
        }
    }
}
