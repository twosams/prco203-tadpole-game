﻿using UnityEngine;
using System.Collections;

public class brokePad : MonoBehaviour {

    float force = 100f;
    float lerpVal = 0;
    float lerpRate = 1;
    
	void Start ()
    {
        
        transform.GetChild(0).GetComponent<Rigidbody2D>().AddForce(new Vector2(-1, 1) * force);
        transform.GetChild(1).GetComponent<Rigidbody2D>().AddForce(new Vector2(-1, -1) * force);
        transform.GetChild(2).GetComponent<Rigidbody2D>().AddForce(new Vector2(1, -1) * force);
        transform.GetChild(3).GetComponent<Rigidbody2D>().AddForce(new Vector2(1, 1) * force);
	}
    
    void Update()
    {
        if (lerpVal < 1)
        {
            lerpVal += Time.deltaTime * lerpRate;
            Debug.Log("Lerping");
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(transform.GetChild(0).GetComponent<SpriteRenderer>().color.r, transform.GetChild(0).GetComponent<SpriteRenderer>().color.b, transform.GetChild(0).GetComponent<SpriteRenderer>().color.g, 1 - lerpVal);
            transform.GetChild(1).GetComponent<SpriteRenderer>().color = new Color(transform.GetChild(1).GetComponent<SpriteRenderer>().color.r, transform.GetChild(1).GetComponent<SpriteRenderer>().color.b, transform.GetChild(1).GetComponent<SpriteRenderer>().color.g, 1 - lerpVal);
            transform.GetChild(2).GetComponent<SpriteRenderer>().color = new Color(transform.GetChild(2).GetComponent<SpriteRenderer>().color.r, transform.GetChild(2).GetComponent<SpriteRenderer>().color.b, transform.GetChild(2).GetComponent<SpriteRenderer>().color.g, 1 - lerpVal);
            transform.GetChild(3).GetComponent<SpriteRenderer>().color = new Color(transform.GetChild(3).GetComponent<SpriteRenderer>().color.r, transform.GetChild(3).GetComponent<SpriteRenderer>().color.b, transform.GetChild(3).GetComponent<SpriteRenderer>().color.g, 1 - lerpVal);
        }
        else
        {
            Destroy(gameObject);
        }
    }     
}
